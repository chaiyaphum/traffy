<!DOCTYPE html>
<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />  
  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAmA1clU8w17qVqwTuqx6jndAtBrrfR_LM&sensor=false"></script>
  <script>
  var geocoder;
  var map;
  var marker;

  function initialize()
  {
    var latlng = new google.maps.LatLng(-33.897, 150.099);
    var myOptions = {
      zoom: 9,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.TERRAIN
    };
    map = new google.maps.Map(document.getElementById("map"), myOptions);

    var rendererOptions = { map: map };
    directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

////////////////////////////////////////////////////////////////////////////////////

var org=new google.maps.LatLng(13.334973,100.968375);
var point1=new google.maps.LatLng(13.33279,100.964565);
var point2=new google.maps.LatLng(13.326573,100.960095);
var point3=new google.maps.LatLng(13.324325,100.958041);
var point4=new google.maps.LatLng(13.318956,100.959989);
var point5=new google.maps.LatLng(13.325175,100.956993);
var point6=new google.maps.LatLng(13.332298,100.951708);
var point7=new google.maps.LatLng(13.335341,100.956948);
var point8=new google.maps.LatLng(13.338928,100.959455);

var dest=new google.maps.LatLng(13.335271,100.970493);
var wps=[{location:point1},{location:point2},{location:point3},{location:point4},{location:point5},{location:point6},{location:point7},{location:point8}];

///////////////////////////////////////////////////////////////////////////////////

var request = {
  origin: org,
  destination: dest,
  waypoints: wps,
  travelMode: google.maps.DirectionsTravelMode.DRIVING
};

directionsService = new google.maps.DirectionsService();
directionsService.route(request, function(response, status) {
  if (status == google.maps.DirectionsStatus.OK) {
    directionsDisplay.setDirections(response);
  }
  else
    alert ('failed to get directions');
});
}

</script>
</head>

<body onload="initialize();">
  <div id="map" style="width: 1200px; height: 650px;"></div>
</body>
</html>