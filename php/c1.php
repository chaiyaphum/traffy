<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PostgreSQL SELECT Example 1</title>
</head>
<body> 
	<?php
	$db = pg_connect("host=localhost port=5433 dbname=test user=postgres password=14112534x")
		or die ("Could not connect to server\n"); ;
	
	$result = pg_query($db,"SELECT * FROM public.table1");

	echo "<table border='1'>";
	while($row=pg_fetch_assoc($result))
	{
		echo "<tr>";
		echo "<td align='center' width='200'>" . $row['id'] . "</td>";
		echo "<td align='center' width='200'>" . $row['value'] . "</td>";
		echo "</tr>";
	}
	echo "</table>";
	?>
</div>
</body>
</html>
