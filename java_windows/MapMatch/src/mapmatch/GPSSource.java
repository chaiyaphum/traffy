package mapmatch;

import java.util.LinkedList;

public interface GPSSource {
	public LinkedList<GPSpnt> getGPS();
	public LinkedList<GPSpnt> getGPS(int limit);
}
