package mapmatch;
import java.sql.Connection;

import org.postgis.LineString;

public class LinkObj extends LineString{
	
	private static final long serialVersionUID = 1L;
	private int linkID;
	private boolean oneWay;
	private double length;
	
	public LinkObj(){
		super();
	}
	public LinkObj(Connection conn,int id){
		linkID=id;
		length=Util.getLinkLength(conn,id);
		
	}
	public LinkObj(LineString line ,int id,boolean oneWay){
		super(line.getPoints());
		linkID=id;
		this.oneWay=oneWay;
		
	}
	public int getLinkID(){
		return linkID;
	}
	public boolean isOneWay(){
		return oneWay;
	}
	public LinkObj clone(){
		LinkObj newLink=new LinkObj(new LineString(getPoints()),linkID,oneWay);
		newLink.length=this.length;
		return newLink;
	}
	public double getLength(){
		return length;
	}
	public void setLength(int length){
		this.length=length;
	}
	public void setID(int id){
		this.linkID=id;
	}
}
