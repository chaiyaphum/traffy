package mapmatch;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

public class DBUtil {
//This part of code by Craig Ringer to check for transient sql exception
private static final List<String> transientStates = Arrays.asList( 
	             "08",   // Connection exceptions - refused, broken, etc 
	             "53",   // Insufficient resources - disk full, etc 
	             "57P0", // Db server shutdown/restart 
	             "40001",// Serialization failure 
	             "40P01"// Deadlock detected 
	             ); 

/** 
  * Attempt to figure out whether a given database error could be 
  * transient 
  * and might be worth a retry. Detects things like network timeouts, 
  * transaction deadlocks, serialization failures, connection drops, 
  * etc. 
  * 
  * @param e Exception thrown by persistence provider 
  * @return True if the error might be transient 
  */ 
 public static boolean isDbErrorTransient(SQLException se) { 
      
     if (se == null) 
         return false; 
     final String sqlState = se.getSQLState(); 
     for (String s : transientStates) { 
         //Logging for sql transient error
    	 
         if (sqlState.startsWith(s)) {
        	 Debugging.logDebug("Transient DB error Detect");
        	 return true; 
         }
             
     } 
     Debugging.logDebug("non-Transient DB error Detect");
     return false; 
 } 

public static void close(ResultSet rs){
	try{
		if(rs!=null){
			rs.close();
		}
	}catch(SQLException  e){
		Debugging.logDebug("Close ResultSet error DBUtil:line 55");
	}
}
public static void close(Statement s){
	try{
		if(s!=null){
			s.close();
		}
	}catch(SQLException  e){
		Debugging.logDebug("Close ResultSet error DBUtil:line 64");
	}
}
public static void close(Connection c){
	try{
		if(c!=null){
			c.close();
		}
	}catch(SQLException  e){
		Debugging.logDebug("Close ResultSet error DBUtil:line 73");
	}
}
public static Connection connectGPSDB(){
	return connect(Config.GPS_DB_HOST, Config.GPS_DB_USER,
			Config.GPS_DB_PASS);
}
public static Connection connectTTDB(){
	return connect(Config.TT_DB_HOST, Config.TT_DB_USER,
			Config.TT_DB_PASS);
}
public static Connection connectLinkDB(){
	return connect(Config.LINK_DB_HOST, Config.LINK_DB_USER,
			Config.LINK_DB_PASS);
}
public static Connection connect(String host,String user,String pass){
	int maxRetry=Config.MAX_RECONNECT;
	boolean success=false;
	Connection conn=null;
	do{
		try{
			conn=DriverManager.getConnection(host, user, pass);
			maxRetry=0;
			success=true;
		}catch(SQLException  e){
			//Check if the error is transient(reconnect may work)
			if(isDbErrorTransient(e)){
				//error is transient 
				//minus retries
				//print error message
				maxRetry--;
				System.out.println("Transient Connection error >"
									+maxRetry+" Retries left : " +
									"Wait :"+Config.RECONNECT_WAIT+" s");
				try{
					Thread.sleep(1000*Config.RECONNECT_WAIT);
				}catch(InterruptedException ie){
					//no need to do any thing
				}
			}else{
				Debugging.logDebug("Conncetion to server fail program is closed");
				System.exit(1);
			}
		}
		
	}while(maxRetry>0);
	if(success)
		return conn;
	else{
		Debugging.logDebug("Conncetion to server fail program is closed");
		System.exit(1);
		return null;
	}
		
}

}

