package mapmatch;
/**
 * This is the configuration constant and configuration need for travel time estimation and map matching
 * 
 **/
import java.util.TreeMap;
public class Config {

	// Map matching constant variables
	public static final int GPS_ERROR_RADIUS_METER = 38; //GPS Error and half of width of largest road
	public static final double MIN_PROB=0.000;
	public static final double DISTANCE_MEAN=0;
	public static final double DISTANCE_VARIENCE=15.4336;
	public static final double DISTANCE_ALPHA=48.7076;
	public static double DISTANCE_WEIGHT=0.9;
	public static final double DEGREE_MEAN=-0.406621;
	public static final double DEGREE_VARIENCE=0.290006;
	public static final double DEGREE_ALPHA=0.860246;
	public static double DEGREE_WEIGHT=0.1;
	public static double LINK_CHANGE_PENALTY=0.5;
	public static String AGGREGRATE_METHOD=//"sum";
										   "multiply";	
	public static int  HMM_WINDOW=5;
	
	//GPS DATABASE PARAMETERS
	public static final String GPS_DB_USER="game";
	public static final String GPS_DB_PASS="Timekeeper@8";
	//public static final String GPS_DB_HOST="jdbc:postgresql://203.185.65.42/gpsarchiver"; // real host
	//public static final String GPS_DB_HOST="jdbc:postgresql://203.185.65.41/Traffic_Data";
	//public static final String GPS_DB_HOST="jdbc:postgresql://10.226.44.90/Traffic_Data"; 	//p' kwang
	public static final String GPS_DB_HOST="jdbc:postgresql://127.0.0.1:5432/Traffic_Data";
	//public static final String GPS_DB_HOST="jdbc:postgresql://203.185.67.163/Traffic_Data";
	public static final String GPS_SCHEMA = //"gps2";
											//"probe_temp";
											"gps_data";
											//"probe";
	public static final String GPS_TABLE = //"testfilteredgps";
										   "test_gps_1min";
										   //"gps_cache";
	public static final String GPS_NOTICE_NAME="gps_inserted";
	public static final long GPS_WAITTING=70; //gps waiting time in ms 
											
	
	
	//LINK DATABASE PARAMETERS
	public static final String LINK_DB_USER="game";
	public static final String LINK_DB_PASS="Timekeeper@8";
	public static final String LINK_DB_HOST="jdbc:postgresql://127.0.0.1:5432/Traffic_Data"; 	//local host
	//public static final String LINK_DB_HOST="jdbc:postgresql://203.185.65.41/Traffic_Data"; // real host				
	public static final String LINK_SCHEMA = //"gps_label";
											 "road_data";
											//"link";
	public static final String LINK_TABLE = "roadmot_expway";
											//"test_road";
	
	
	
		
	
	//TRAVEL TIME DATABASE PARAMETERS
	public static final String TT_DB_USER="game";
	public static final String TT_DB_PASS="Timekeeper@8";
	public static final String TT_DB_HOST="jdbc:postgresql://127.0.0.1:5432/Traffic_Data"; 		//Real host
	//public static final String TT_DB_HOST="jdbc:postgresql://203.185.65.41/Traffic_Data";
	public static final String TT_SCHEMA = //"real_time_traveltime";		//REAL
										   "traveltime2";   //LOCAL
	public static final String SPACE_MEAN_TT_TEMP = "space_mean_speed_temp";
	public static final String SPACE_MEAN_TT_ARCH = "space_mean_speed_archive";
	public static final String SPOT_TT_TEMP= "spot_speed_temp";
	public static final String SPOT_TT_Archive="spot_speed_arc_stat_1min";
	public static final String AVG_SPOT_TT_TEMP="average_spot_speed_temp";
	
	// Other config used in calculation
	public static final int    TRAVELTIME_TIMEOUT=10;//time between invoke of removeoldsource
	public static final double MIN_DISPLACEMENT=10;
	public static final double MIN_SPEED=5;
	public static final double DOUBLE_MIN_DIFF=0.001;
	public static final int UNI_LINK_MARGIN=1000000;
	public static final boolean INCLUDE_INCONSISTENT_POINT=true;
	public static final boolean USE_MAP_INDEX=true;
	public static final TreeMap<String,Integer>  VTYPE_MAP=new TreeMap<String,Integer>(){
																	{ 	put("taxi",1);
																		put("van",2);
																		put("bus",3);
																		put("feet",4);
																		put("public",5);
																	}
																};
	   
	//common variable
	public static final String ERROR_LOG_FILE = "Error log.txt";
	public static final boolean IS_DEBUG=false;
	public static final int MAX_RECONNECT=20;
	public static final int RECONNECT_WAIT=30; //waitting time before trying to reconnect in second
							

}