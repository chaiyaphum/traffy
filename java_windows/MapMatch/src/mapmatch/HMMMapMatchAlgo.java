package mapmatch;

import hmm.EmissionProb;
import hmm.HMM;
import hmm.Observation;
import hmm.ViterbiCalculator;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;

//import org.opengis.geometry.coordinate.LineString;
//import org.opengis.geometry.primitive.Point;

import org.postgis.LineString;
import org.postgis.PGgeometry;
import org.postgis.Point;

/*
 import org.postgis.LineString;
 import org.postgis.PGgeometry;
 import org.postgis.Point;
 /*
 * 
 */
/**
 * MapMatchAlgo is act like the processing heart of Map Matching algorithm. The
 * calculation will be done here and return the results consist of linkID and
 * lampda.
 * */

public class HMMMapMatchAlgo {

	public final double PI_DIV_180 = Math.PI / 180;

	public final double PI_MUL_2 = Math.PI * 2;
	public final double RAD_TO_DEGREE = 180 / Math.PI;

	private static Connection conn_LinkDB;
	private HMM hmm;
	private static Statement statement;
	private ArrayList<Observation> obsCandidateList;
	private EmissionProb eProb;
	private ArrayList<GPSpnt> GPSList;
	private static SimpleDateFormat df = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.SSS");

	public HMMMapMatchAlgo() {

		conn_LinkDB = DBUtil.connectLinkDB();
		boolean fail = true;
		// Reconnect when it fail
		do {
			try {
				statement = conn_LinkDB.createStatement();
				fail = false;

			} catch (SQLException e) {
				e.printStackTrace(System.err);
				DBUtil.close(statement);
				DBUtil.close(conn_LinkDB);
				Debugging
						.logDebug("Create Statement for Link DB error HMMMapMatchAlgo line 51");
				if (DBUtil.isDbErrorTransient(e)) {
					conn_LinkDB = DBUtil.connectLinkDB();
				} else {
					System.exit(1);
				}
			}
		} while (fail);
		hmm = new HMM();
		eProb = new EmissionProb();
		GPSList = new ArrayList<GPSpnt>();

	}

	public HMMMapMatchAlgo(Connection conn) {
		conn_LinkDB = conn;
		boolean fail = true;
		do {
			try {
				statement = conn_LinkDB.createStatement();
				fail = false;
			} catch (SQLException e) {
				e.printStackTrace(System.err);
				DBUtil.close(conn_LinkDB);
				Debugging
						.logDebug("Create Statement for Link DB error HMMMapMatchAlgo line 77");
				if (DBUtil.isDbErrorTransient(e)) {

					conn_LinkDB = DBUtil.connectLinkDB();
				} else {
					System.exit(1);
				}
			}
		} while (fail);
		hmm = new HMM();
		eProb = new EmissionProb();
		GPSList = new ArrayList<GPSpnt>();

	}

	public TTGPSpnt[] mapmatch(GPSpnt p) {
		// DateFormat df =DateFormat.getDateInstance();
		if (p != null) {
			Debugging.printlnDebug(" Hmm size = " + hmm.stepNum());
			if (p.heading > 0)
				addPointWithDir(p);

			else
				addPointNodir(p);

			if (hmm.stepNum() == Config.HMM_WINDOW) {
				Debugging.printlnDebug(hmm.toString());

				return createResultList();
			} else
				return null;
		} else {// p==null is terminate condition
			if (hmm.stepNum() > 0) {
				return createResultList();
			} else
				return null;
		}

	}

	public TTGPSpnt[] mapmatch(PointQueue pq) {
		TTGPSpnt[] result = null;
		// add result from queue to hmm until it full and return result
		while (!pq.isEmpty() && result == null) {
			result = mapmatch(pq.removeFirst());
		}
		// add the rest of the point from queue (which don't return any result
		// for sure)
		while (!pq.isEmpty()) {
			mapmatch(pq.removeFirst());
		}
		return result;
	}

	private TTGPSpnt[] createResultList() {
		ViterbiCalculator vc = new ViterbiCalculator(hmm);
		Observation[] rs = vc.stateSequence();
		TTGPSpnt[] resultList = new TTGPSpnt[rs.length];
		ResultSet r = null;
		int count = 0;
		for (Observation obs : rs) {

			// find tentative snapped point
			Point start = (Point) obs.getNSL().getStartPoint();
			Point end = (Point) obs.getNSL().getEndPoint();
			Point gpsPoint = (Point) obs.getPoint();
			LineString theLineString = (LineString) obs.getNL();
			double lampda = 0;
			// Snap point can be find by finding the projection of point onto
			// that vector
			// However if the projection of point is futher away from that line
			// string
			// It is assume to be the nearest point on that line string(the
			// nearest end point) instead
			Point vecA = new Point(end.x - start.x, end.y - start.y);
			Point vecB = new Point(gpsPoint.x - start.x, gpsPoint.y - start.y);
			Point proj_vecA_vecB = project(vecA, vecB);
			Point snpPoint = new Point(start.x + proj_vecA_vecB.x, start.y
					+ proj_vecA_vecB.y);

			StringBuffer theLineString_sb = new StringBuffer();
			StringBuffer snpPoint_sb = new StringBuffer();
			theLineString.outerWKT(theLineString_sb);
			snpPoint.outerWKT(snpPoint_sb);
			boolean fail = true;
			do {

				try {
					try {
						statement = conn_LinkDB.createStatement();
						r = statement
								.executeQuery(String
										.format("select st_line_locate_point(st_setsrid('%s'::geometry,4326), st_setsrid('%s'::geometry,4326))",
												theLineString_sb, snpPoint_sb));
						r.next();
						lampda = r.getDouble(1);
						fail = false;
					} finally {
						DBUtil.close(r);
						DBUtil.close(statement);
					}
				} catch (SQLException e) {
					e.printStackTrace(System.err);
					DBUtil.close(conn_LinkDB);
					Debugging
							.logDebug("SQL Error in pntToCurveMapmatch line 177");
					if (DBUtil.isDbErrorTransient(e)) {

						conn_LinkDB = DBUtil.connectLinkDB();
					} else {
						System.exit(1);
					}
				}
			} while (fail);
			resultList[count] = new TTGPSpnt(GPSList.get(count), lampda,
					obs.getLinkID(), snpPoint, obs.pos_prob, obs.heading_prob);
			count++;

		}// end for(Observation obs : rs)
			// reset hmm and gpsList
		hmm = new HMM();
		GPSList = new ArrayList<GPSpnt>();
		return resultList;
	}

	/**
	 * if no candidate exists, meaning matching failed, then the outputObjectArr
	 * is {-1,-1}
	 * 
	 * @param inputObjectArr
	 *            an input array -- lat : double, lng : double, course : int
	 * @param outputObjectArr
	 *            an output array -- link_id : int, lampda : double
	 */
	private void addPointWithDir(GPSpnt p) {

		// two outputs initialized
		obsCandidateList = new ArrayList<Observation>();

		double lat = p.lat;
		double lng = p.lng;
		Point gpsPoint = new Point(lng, lat);

		Debugging
				.printlnDebug(String.format("%f, %f, %f", lat, lng, p.heading));
		LinkedList<LinkObj> candidateList = null;
		// this function get link near the gps point

		// TODO edit back to normal matchs
		// candidateList = getLinkWithinRange(lat, lng);
		// -----For correct matched
		candidateList = getLinkWithinRange(lat, lng, p);

		Debugging.printlnDebug("candidate size = " + candidateList.size());

		// no candidate -> matching failed
		if (candidateList.size() == 0) {
			return;
		}

		for (LinkObj link : candidateList) {
			// find min distance -- node(which is a member of line) to
			// point(identified variable)

			/*
			 * //Debug Debugging.printlnDebug(
			 * "=============================================\n" +
			 * "Start matching link : "+link.getLinkID());
			 */
			double min = Double.MAX_VALUE;
			int minDistIndex = 0;
			for (int j = 0; j < link.numPoints(); j++) {
				double dist = link.getPoints()[j].distance(gpsPoint);
				if (dist < min) {
					min = dist;
					minDistIndex = j;
				}
			}

			Point start, end, pStart;
			if (minDistIndex != link.numPoints() - 1 && minDistIndex > 0) {

				// pre-assign (presented in Yang's algorithm)
				pStart = link.getPoints()[minDistIndex - 1];
				start = link.getPoints()[minDistIndex];
				end = link.getPoints()[minDistIndex + 1];

				// adjustment checking (swap: start - end)
				// vector start->gpspoint
				Point vSP = new Point(gpsPoint.x - start.x, gpsPoint.y
						- start.y);
				// vector start -> previous start
				Point vSPS = new Point(pStart.x - start.x, pStart.y - start.y);
				// vector start->end
				Point vSE = new Point(end.x - start.x, end.y - start.y);

				double costheta = getCosTheta(vSP, vSE);
				double costheta_ = getCosTheta(vSP, vSPS);

				// crucial criteria here
				// if this sub-linestring form a larger angle to the point
				if (costheta < costheta_) {
					start = link.getPoints()[minDistIndex - 1];
					end = link.getPoints()[minDistIndex];
					// Debug
					Debugging.printlnDebug("Candidate index : "
							+ (minDistIndex - 1) + "," + minDistIndex);
				}
				// Debug
				Debugging.printlnDebug("Candidate index : " + (minDistIndex)
						+ "," + (minDistIndex + 1));
			}
			// for the last vertex of LineString, there is only choice
			else if (minDistIndex == link.numPoints() - 1) {
				start = link.getPoints()[minDistIndex - 1];
				end = link.getPoints()[minDistIndex];
				// Debug
				Debugging.printlnDebug("Candidate index : "
						+ (minDistIndex - 1) + "," + minDistIndex);
			} else { // minDistIndex ==0
				start = link.getPoints()[minDistIndex];
				end = link.getPoints()[minDistIndex + 1];
			}

			double probability = 0;
			int linkID = -1;
			LineString ls = null;
			double error_pos = 0;
			double error_heading = 0;
			Debugging.printlnDebug("Link_ID =" + link.getLinkID());
			if (!link.isOneWay()) {
				// If two-way then a reverse SubLineString will also be
				// processed
				// If reverse sublinestring is choosen Config.UNI_LINK_MARGIN
				// will be added
				// to distinguish them from normal link
				double degreeForward = getDegreeDiff(start, end, p);
				double degreeBackward = getDegreeDiff(end, start, p);

				if (Math.abs(degreeForward) < Math.abs(degreeBackward)) {
					error_pos = getDistancePointToLine(start, end, gpsPoint);
					error_heading = degreeForward;
					probability = eProb.getProb(error_pos, degreeForward);

					ls = new LineString(new Point[] { start, end });
					linkID = link.getLinkID();
				} else {
					error_pos = getDistancePointToLine(end, start, gpsPoint);
					error_heading = degreeBackward;
					probability = eProb.getProb(error_pos, degreeBackward);
					ls = new LineString(new Point[] { end, start });
					linkID = link.getLinkID() + Config.UNI_LINK_MARGIN;
				}
			} else {
				error_pos = getDistancePointToLine(start, end, gpsPoint);
				error_heading = getDegreeDiff(start, end, p);
				probability = eProb.getProb(error_pos, error_heading);
				ls = new LineString(new Point[] { start, end });
				linkID = link.getLinkID();
			}
			double pos_prob = eProb.getDistanceProb(error_pos);
			double heading_prob = eProb.getDegreeProb(error_heading);
			if (probability > Config.MIN_PROB) {

				Debugging.printlnDebug("Prob = " + probability);
				Observation o = new Observation(probability, linkID, ls, link,
						gpsPoint);
				o.pos_prob = pos_prob;
				o.heading_prob = heading_prob;
				obsCandidateList.add(o);

			}

			Debugging.printlnDebug("Position Prob = " + pos_prob
					+ " : Heading Prob=" + heading_prob);

		}// End for
		if (obsCandidateList.size() > 0) {
			Debugging.printlnDebug(obsCandidateList.size() + "  points added");
			hmm.addTimeStep(obsCandidateList);
			GPSList.add(p);
		}

	}// mapMatchWithDir end

	/**
	 * work same as pntToCurveMapmatch but do not consider heading
	 * 
	 * @param p
	 *            raw GPS point
	 * @return
	 */
	private void addPointNodir(GPSpnt p) {

		// two outputs initialized
		obsCandidateList = new ArrayList<Observation>();

		double lat = p.lat;
		double lng = p.lng;
		Point gpsPoint = new Point(lng, lat);

		Debugging
				.printlnDebug(String.format("%f, %f, %f", lat, lng, p.heading));
		LinkedList<LinkObj> candidateList = null;
		// this function get link near the gps point

		// TODO edit back to normal match
		// candidateList = getLinkWithinRange(lat, lng);
		// -----For correct matched
		candidateList = getLinkWithinRange(lat, lng, p);

		Debugging.printlnDebug("candidate size = " + candidateList.size());

		// no candidate -> matching failed
		if (candidateList.size() == 0) {
			return;
		}

		for (LinkObj link : candidateList) {
			// find min distance -- node(which is a member of line) to
			// point(identified variable)

			/*
			 * //Debug Debugging.printlnDebug(
			 * "=============================================\n" +
			 * "Start matching link : "+link.getLinkID());
			 */
			double min = Double.MAX_VALUE;
			int minDistIndex = 0;
			for (int j = 0; j < link.numPoints(); j++) {
				double dist = link.getPoints()[j].distance(gpsPoint);
				if (dist < min) {
					min = dist;
					minDistIndex = j;
				}
			}

			Point start, end;
			if (minDistIndex != link.numPoints() - 1) {

				// pre-assign (presented in Yang's algorithm)
				start = link.getPoints()[minDistIndex];
				end = link.getPoints()[minDistIndex + 1];

				// adjustment checking (swap: start - end)
				// vector start->gpspoint
				Point vSP = new Point(gpsPoint.x - start.x, gpsPoint.y
						- start.y);
				// vector end->gpspoint
				Point vEP = new Point(gpsPoint.x - end.x, gpsPoint.y - end.y);
				// vector start->end
				Point vSE = new Point(end.x - start.x, end.y - start.y);
				// vector end->start
				Point vES = new Point(start.x - end.x, start.y - end.y);

				double costheta = getCosTheta(vSP, vSE);
				double costheta_ = getCosTheta(vEP, vES);

				// crucial criteria here

				if (!(costheta > 0 && costheta_ > 0) && minDistIndex > 0) {
					start = link.getPoints()[minDistIndex - 1];
					end = link.getPoints()[minDistIndex];
					// Debug
					Debugging.printlnDebug("Candidate index : "
							+ (minDistIndex - 1) + "," + minDistIndex);
				}
				// Debug
				Debugging.printlnDebug("Candidate index : " + (minDistIndex)
						+ "," + (minDistIndex + 1));
			}
			// for the last vertex of LineString, there is only choice
			else {
				start = link.getPoints()[minDistIndex - 1];
				end = link.getPoints()[minDistIndex];
				// Debug
				Debugging.printlnDebug("Candidate index : "
						+ (minDistIndex - 1) + "," + minDistIndex);
			}
			double error_pos = this
					.getDistancePointToLine(start, end, gpsPoint);
			double probability = eProb.getProb(error_pos, 360.0);
			LineString ls = new LineString(new Point[] { start, end });
			if (probability > Config.MIN_PROB) {
				Debugging.printlnDebug("Prob = " + probability);
				Observation o = new Observation(probability, link.getLinkID(),
						ls, link, gpsPoint);
				o.pos_prob = eProb.getDistanceProb(error_pos);
				o.heading_prob = eProb.getDegreeProb(360.0);
				Debugging.printlnDebug("Position Prob = " + o.pos_prob
						+ " : Heading Prob" + o.heading_prob);
				obsCandidateList.add(o);
			}

		}// End for
		if (obsCandidateList.size() > 0) {
			hmm.addTimeStep(obsCandidateList);
			GPSList.add(p);
		}

	}

	/**
	 * 
	 * @param thePoint
	 * @param candidate
	 * @return LineString[2]- [0]=nearest sub linestring [1]=nearest LinkObj
	 */
	/*
	 * private LineString[] findClosestSubLineString(Point thePoint,
	 * LinkedList<LinkObj> candidate){ double minDist = Double.MAX_VALUE; int
	 * the_id = -1; LineString subLineString = null; LinkObj nearestLink = null;
	 * // for each subLine in each line for (LinkObj curLink:candidate) { for
	 * (int j = 0; j < curLink.numPoints() - 1; j++) { double d =
	 * getDistancePointToLine(curLink.getPoint(j), curLink.getPoint(j + 1),
	 * thePoint); if (d < minDist) { Point start = curLink.getPoint(j); Point
	 * end = curLink.getPoint(j + 1); Point vA = new Point(thePoint.x - start.x,
	 * thePoint.y - start.y); Point vB = new Point(thePoint.x - end.x,
	 * thePoint.y - end.y); double costheta = getCosTheta(vA, new Point( end.x -
	 * start.x, end.y - start.y)); double costheta_ = getCosTheta(vB, new
	 * Point(start.x - end.x, start.y - end.y));
	 * 
	 * // crucial criteria here //check whether sub line and point form an acute
	 * triangle if (costheta > 0 && costheta_ > 0) { Point[] pArr =
	 * {curLink.getPoint(j),curLink.getPoint(j + 1)}; minDist = d; subLineString
	 * = new LineString(pArr); nearestLink = curLink; } } } }
	 * 
	 * // wrap them up
	 * 
	 * return new LineString[]{subLineString,nearestLink}; }
	 */

	/**
	 * project vector b on vector a
	 * 
	 * @param a
	 *            the basis vector
	 * @param b
	 *            the vector to be projected
	 * @return
	 */
	private Point project(Point a, Point b) {
		double factor = (a.x * b.x + a.y * b.y) / (a.x * a.x + a.y * a.y);
		double cosTheta = getCosTheta(a, b);
		// if angle is obtuse angle return size of vector start to point instead
		// of projection
		if (cosTheta < 0) {
			return new Point(0, 0);
		} else {
			Point proj = new Point(factor * a.x, factor * a.y);
			double sizeProjsq = proj.x * proj.x + proj.y * proj.y;
			double sizeVecAsq = a.x * a.x + a.y * a.y;
			// if projection of point is futher than end point use size of
			// vector end to point instead
			if (sizeProjsq > sizeVecAsq) {
				return a;
			}
			return proj;
		}

	}

	/**
	 * calculate distance of the input's coordinate
	 * 
	 * @param start
	 * @param end
	 * @param the_point
	 * @return
	 */
	private double getDistancePointToLine(Point start, Point end,
			Point the_point) {
		// Class Point is used as a vector here
		Point vecA = new Point(end.x - start.x, end.y - start.y);
		Point vecB = new Point(the_point.x - start.x, the_point.y - start.y);
		double cosTheta = getCosTheta(vecA, vecB);
		// if angle is obtuse angle return size of vector start to point instead
		// of projection
		if (cosTheta < 0) {
			return Math.sqrt(vecB.x * vecB.x + vecB.y * vecB.y);
		} else {
			Point proj = project(vecA, vecB);
			double sizeProjsq = proj.x * proj.x + proj.y * proj.y;
			double sizeVecAsq = vecA.x * vecA.x + vecA.y * vecA.y;
			// if projection of point is futher than end point use size of
			// vector end to point instead
			if (sizeProjsq > sizeVecAsq) {
				Point vecEP = new Point(the_point.x - end.x, the_point.y
						- end.y);
				return Math.sqrt(vecEP.x * vecEP.x + vecEP.y * vecEP.y);
			}
			Point x = new Point(vecB.x - proj.x, vecB.y - proj.y);
			return Math.sqrt(x.x * x.x + x.y * x.y);
		}
	}

	/**
	 * 
	 * @param a1
	 *            an angle in azimuth coordinate
	 * @param a2
	 *            another angle in azimuth coordinate
	 * @return diff value in a range of [0,Pi]
	 */
	private double getRadDiffAngle(double a1, double a2) {
		double diff_temp = a1 - a2;
		if (diff_temp > Math.PI) {
			return diff_temp - (PI_MUL_2);
		} else if (diff_temp < -Math.PI) {
			return diff_temp + (PI_MUL_2);
		}
		return diff_temp;
	}

	private double getCosTheta(Point a, Point b) {
		double sizeA_sqr = a.x * a.x + a.y * a.y;
		double sizeB_sqr = b.x * b.x + b.y * b.y;
		double dot = a.x * b.x + a.y * b.y;
		return dot / Math.sqrt(sizeA_sqr * sizeB_sqr);
	}

	private double getDegreeDiff(Point start, Point end, GPSpnt pnt) {
		double rad = getAzimuth(start, end);
		// this is absolutely wrong!
		// double diff = Math.abs(rad - heading*Math.PI/180);

		double heading_in_rad = pnt.heading * PI_DIV_180;
		double diff = getRadDiffAngle(heading_in_rad, rad);
		return diff * RAD_TO_DEGREE;

	}

	private static LinkedList<LinkObj> getLinkWithinRange(double lat, double lng) {
		LinkedList<LinkObj> candidateLink = new LinkedList<LinkObj>();

		// note: longtitude => x, latitude => y
		ResultSet r = null;
		String searchFunction = null;
		if (Config.USE_MAP_INDEX)
			searchFunction = "find_link";
		else
			searchFunction = "_find_link";

		String sql = String.format("select gid,oneway,the_geom " + "from %s."
				+ searchFunction + "('%s.%s',%f,%f) ;", Config.LINK_SCHEMA,
				Config.LINK_SCHEMA, Config.LINK_TABLE, lng, lat);

		// Debugging.printlnDebug(sql);
		boolean fail = true;
		do {

			try {
				try {
					statement = conn_LinkDB.createStatement();
					r = statement.executeQuery(sql);

					while (r != null && r.next()) {
						PGgeometry geom = (PGgeometry) r.getObject("the_geom");
						LineString theLine = (LineString) geom.getGeometry();
						boolean oneWay = "FT".equals(r.getString("oneway"));
						candidateLink.add(new LinkObj(theLine, r.getInt("gid"),
								oneWay));
					}
					fail = false;

				} finally {
					DBUtil.close(r);
					DBUtil.close(statement);
				}
			} catch (SQLException e) {
				e.printStackTrace(System.err);
				DBUtil.close(conn_LinkDB);
				Debugging
						.logDebug("SQL Error in HMMMapMatchAlgo.getLinkWithinRange line 643 \n"
								+ "SQL :" + sql);
				if (DBUtil.isDbErrorTransient(e)) {

					conn_LinkDB = DBUtil.connectLinkDB();
				} else {
					System.exit(1);
				}
			}
		} while (fail);

		return candidateLink;
	}

	private static LinkedList<LinkObj> getLinkWithinRange(double lat,
			double lng, GPSpnt p) {
		LinkedList<LinkObj> candidateLink = new LinkedList<LinkObj>();

		// note: longtitude => x, latitude => y
		ResultSet r = null;
		String searchFunction = null;
		if (Config.USE_MAP_INDEX)
			searchFunction = "find_link";
		else
			searchFunction = "_find_link";

		String sql = String.format("select gid,oneway,the_geom " + "from %s."
				+ searchFunction + "('%s.%s',%f,%f,'%s') ;",
				Config.LINK_SCHEMA, Config.LINK_SCHEMA, Config.LINK_TABLE, lng,
				lat, df.format(p.rectime));

		Debugging.printlnDebug(sql);
		boolean fail = true;
		do {
			try {
				try {
					statement = conn_LinkDB.createStatement();
					r = statement.executeQuery(sql);
					while (r != null && r.next()) {
						PGgeometry geom = (PGgeometry) r.getObject("the_geom");
						LineString theLine = (LineString) geom.getGeometry();
						boolean oneWay = "FT".equals(r.getString("oneway"));
						candidateLink.add(new LinkObj(theLine, r.getInt("gid"),
								oneWay));
					}
					r.close();
					fail = false;
				} finally {
					DBUtil.close(r);
					DBUtil.close(statement);
				}
			} catch (SQLException e) {
				e.printStackTrace(System.err);
				DBUtil.close(conn_LinkDB);
				Debugging
						.logDebug("SQL Error in getLinkWithinRange: HMMMapMatchAlgo line696 \n"
								+ "SQL :" + sql);
				if (DBUtil.isDbErrorTransient(e)) {

					conn_LinkDB = DBUtil.connectTTDB();
				} else {
					System.exit(1);
				}
			}
		} while (fail);

		return candidateLink;
	}

	public double getAzimuth(Point start, Point end) {
		double rad = -1;
		StringBuffer sbStart = new StringBuffer();
		StringBuffer sbEnd = new StringBuffer();
		start.outerWKT(sbStart);
		end.outerWKT(sbEnd);
		ResultSet r = null;
		boolean fail = true;
		do {
			try {
				try {
					statement = conn_LinkDB.createStatement();
					r = statement
							.executeQuery(String
									.format("select st_azimuth(st_setsrid('%s'::geometry,4326), st_setsrid('%s'::geometry,4326))",
											sbStart, sbEnd));
					r.next();
					rad = r.getDouble(1);
					fail = false;
				} finally {
					DBUtil.close(r);
					DBUtil.close(statement);
				}
			} catch (SQLException e) {
				e.printStackTrace(System.err);
				DBUtil.close(conn_LinkDB);
				Debugging
						.logDebug("Get azimuth Error HMMMapMatchAlgo line 721");
				if (DBUtil.isDbErrorTransient(e)) {
					conn_LinkDB = DBUtil.connectLinkDB();
				} else {
					System.exit(1);
				}
			}
		} while (fail);
		return rad;
	}

	public static double getAzimuth(double startLat, double startLong,
			double endLat, double endLong) {
		double rad = -1;
		ResultSet r = null;

		String sql = String
				.format("select st_azimuth(st_geomFromText(\'point(%f %f)\'),st_geomFromText(\'point(%f %f)\'))",
						startLat, startLong, endLat, endLong);
		// System.out.println(sql);
		boolean fail = true;
		do {
			try {
				try {
					statement = conn_LinkDB.createStatement();
					r = statement.executeQuery(sql);
					r.next();
					rad = r.getDouble(1);
					fail = false;

				} finally {
					DBUtil.close(r);
					DBUtil.close(statement);
				}
			} catch (SQLException e) {
				e.printStackTrace(System.err);
				DBUtil.close(conn_LinkDB);
				Debugging
						.logDebug("Get azimuth error  HMMMapMatchAlgo line 756");
				if (DBUtil.isDbErrorTransient(e)) {

					conn_LinkDB = DBUtil.connectLinkDB();
				} else {
					System.exit(1);
				}
			}
		} while (fail);
		return rad;
	}
}
