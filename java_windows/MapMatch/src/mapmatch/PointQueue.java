package mapmatch;
class Element{
	public GPSpnt p;
	public Element prev;
	public Element next;
	public Element(GPSpnt p){
		this.p=p;
	}
	public void setNext(Element e){
		next=e;
	}
	public  void setPrev(Element e){
		prev=e;
	}
}
public class PointQueue {
	private int size;
	private Element head;
	private Element tail;
	
	public PointQueue(){
		size =0;
		head =null;
		tail=null;
	}
	public void add(GPSpnt p){
		if (size<=0){
			head=new Element(p);
			tail=head;
			size++;
		}
		else {
			Element temp=tail;
			Element newE=new Element(p);
			while(temp!=null){
				//if this point is after current point
				if(newE.p.rectime.compareTo(temp.p.rectime) > 0){
					newE.prev=temp;
					//if temp is not the last point
					if(temp.next!=null){
						newE.next=temp.next;
						newE.next.prev=newE;
					}else{
						tail=newE;
					}
					temp.next=newE;
					size++;
					temp=null;
				//if points is equal we ignore it	
				}else if(temp.p.rectime.equals(newE.p.rectime)){
					if(!temp.p.equals(newE.p)){
						newE.prev=temp;
						//if temp is not the last point
						if(temp.next!=null){
							newE.next=temp.next;
							newE.next.prev=newE;
						}else{
							tail=newE;
						}
						temp.next=newE;
						size++;
					}//else System.out.println("Duplicate Point Ignored");
					temp=null;
				//if new point is less than this point
				}else{
					temp=temp.prev;
					if(temp==null){
						newE.next=head;
						head.prev=newE;
						head=newE;
						size++;
					}
				}
			}
			
		}
		Debugging.printlnDebug(this.toString());
		return;
	}
	public int size(){
		return size;
	}
	public boolean isEmpty(){
		return this.size() ==0;
	}
	public GPSpnt removeLast(){
		Element temp=tail;
		tail=tail.prev;
		//if tail is null there is no point left nothing need to be set
		//else tail.next must set to null because the now the second last point become last point
		if(tail!=null)
			tail.next =null;
		size--;
		return temp.p; 
	}
	public GPSpnt removeFirst(){
		Element temp=head;
		head=head.next;
		//if head is null there is no point left nothing need to be set
		//else head.prev must set to null because the now the second point become first point
		if(head!=null)
			head.prev =null;
		size--;
		return temp.p; 
	}
	public String toString(){
		Element temp = head;
		StringBuffer sb=new StringBuffer();
		while(temp!=null){
			sb.append("["+temp.p.row_id+"] <---> ");
			temp=temp.next;
		}
		return sb.toString();
	}
}

