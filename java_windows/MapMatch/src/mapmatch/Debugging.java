package mapmatch;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Debugging {
	public static final DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	
	public static void logDebug(String s)
	{
		
			File logFile=new File(Config.ERROR_LOG_FILE);
			logDebug(s,logFile);
		
		
	}
	public static void logDebug(String s,File logFile)
	{
		Date now=new Date();
		FileWriter fWriter=null;
		try{
			if(!logFile.exists())
				logFile.createNewFile();
			fWriter= new FileWriter(logFile,true);
			fWriter.write(now+s+"\r\n");
			fWriter.flush();
			
		}catch(IOException e){
			System.err.println("Logging file error");
			System.exit(1);
		}finally{
			try {
				if(fWriter != null){
					fWriter.close();
					fWriter=null;
				}
			}catch(IOException e){
				System.err.println("Close file error");
				System.exit(1);
			}
		}
	}

	public static void printlnDebug(String s) {
		if (Config.IS_DEBUG)
			System.out.println(s);
	}
//	public static void main(String args[]) {
//
//		// checkRunningProcess();
//
//		MapMatcher matcher = new MapMatcher();
//		matcher.setName("matcher");
//		matcher.start();
//
//	}

}
