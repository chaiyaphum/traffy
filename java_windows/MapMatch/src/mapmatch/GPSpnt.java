package mapmatch;
import java.sql.Timestamp;;

/**
 * This class is to make the GPS point to be an object.
 * */

public class GPSpnt {
	
	String source_id;
	
	double lat;
	
	double lng;
	
	double speed;
	
	double heading;
	
	Timestamp rectime;
	String vehicleType;
	GPSpnt prevPnt;
	GPSSource source;

	/**
	 * probe_gps row_id
	 * @uml.property  name="row_id"
	 */
	int row_id;

	public GPSpnt(GPSSource source, int row_id, String source_id, double lat,
			double lng, double speed, double heading, Timestamp rectime,String vt) {
		this.source = source;
		this.row_id = row_id;
		this.source_id = source_id;
		this.lat = lat;
		this.lng = lng;
		this.speed = speed;
		this.heading = heading;
		this.rectime = rectime;
		this.vehicleType=vt;
	}
	public GPSpnt(GPSpnt p){
		this.source = p.source;
		this.row_id = p.row_id;
		this.source_id = p.source_id;
		this.lat = p.lat;
		this.lng = p.lng;
		this.speed = p.speed;
		this.heading = p.heading;
		this.rectime = p.rectime;
		this.vehicleType=p.vehicleType;
	}
	public String toString() {
		return String
				.format("GPSpnt: row_id=%d, source_id=%s, lat=%f, lng=%f, speed=%f, heading=%f, rectime=%s",
						row_id, source_id, lat, lng, speed, heading, rectime.toString());
	}
	public boolean equals(GPSpnt p){
		return 	this.source_id.equals(p.source_id) &&
				this.lat == p.lat &&
				this.lng == p.lng &&
				this.speed == p.speed &&
				this.heading == p.heading &&
				this.rectime.equals(p.rectime) ;
	}
	public void setTime(Timestamp ts){
		rectime=ts;
	}
}
