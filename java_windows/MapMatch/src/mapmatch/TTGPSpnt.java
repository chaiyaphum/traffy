package mapmatch;
import org.postgis.Point;
/**
 * This class is to make the Map-matched GPS point to be an object for Travel time calcultion.
 * */

public class TTGPSpnt extends GPSpnt{

	private double lampda;
	private int linkID;
	private Point matched_point;
	public double pos_prob;
	public double heading_prob;
	//Constant for direction
	public static final int FORWARD=1;
	public static final int BACKWARD=-1;
	public static final int STOP=0;
	
	public TTGPSpnt(GPSpnt p,double lampda,int linkID,Point mPnt) {
		
		super(p.source, p.row_id, p.source_id,p.lat,
				p.lng, p.speed, p.heading, p.rectime,p.vehicleType);
		

		this.lampda=lampda;
		this.linkID=linkID;
		this.matched_point=mPnt;
		pos_prob=-1;
		heading_prob=-1;
	}

	public TTGPSpnt(GPSpnt p,double lampda,int linkID,Point mPnt,double pos_p,double heading_p) {
		
		super(p.source, p.row_id, p.source_id,p.lat,
				p.lng, p.speed, p.heading, p.rectime,p.vehicleType);
		

		this.lampda=lampda;
		this.linkID=linkID;
		this.matched_point=mPnt;
		pos_prob=pos_p;
		heading_prob=heading_p;
	}
	public String toString() {
		return super.toString()+String
				.format("TTGPSpnt:  lampda=%s, linkID=%s , matched_point",
						 lampda, linkID,matched_point);
	}
	public int getLinkID(){
		return linkID;
	}
	public double getLampda(){
		return lampda;
	}
	public void setLampda(double l){
		lampda=l;
	}
	public Point getMPoint(){
		return matched_point;
	}
	
}
