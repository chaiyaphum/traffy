package mapmatch;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author  Tanachart Kohprasurt
 */
public class Util {
	
	public static double getLinkLength(Connection conn,int linkID){
		if(linkID>=-1){
			ResultSet r=null;
			Statement s=null;
			double length=-1;
			if(linkID > Config.UNI_LINK_MARGIN)
				linkID=linkID-Config.UNI_LINK_MARGIN;
			String query=String.format("SELECT shape_leng FROM \"%s\".\"%s\" WHERE \"gid\" = %s",
					Config.LINK_SCHEMA,
					Config.LINK_TABLE,
					linkID);
			boolean fail=true;
			do{
			
				try {	
					try{
					
						s = conn.createStatement();
						r=s.executeQuery(query);
						
						if(r.next())
							length= r.getDouble("shape_leng");
						else 
							length=-1;
						fail=false;
					}finally{
						DBUtil.close(r);
						DBUtil.close(s);
					}
				}catch(SQLException  e){
					e.printStackTrace(System.err);
					DBUtil.close(conn);	
					Debugging.logDebug("getlinkLength fail Util.java line 46");
					if(DBUtil.isDbErrorTransient(e)){
						
						conn=DBUtil.connectLinkDB();
					}else{
						System.exit(1);
					}
				}	
				
			}while(fail);
			return length;
		}else 
			return -1;
		
	}
	/**
	 * 
	 * @param linkID mix type link id
	 * @param direction direction of point
	 * @return unidirectional link id mapping
	 */
	public static int getUniLinkID(int linkID,int direction){
		if(linkID != -1){
			if(direction < 0 )
				return Config.UNI_LINK_MARGIN+linkID;
			else
				return linkID;
						
		}else
			return 0;
		
		
	}
	
	public static void writeLog(String str) throws IOException {
		
	}
	

}
