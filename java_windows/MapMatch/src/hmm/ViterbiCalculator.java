/*
 * Copyright (c) 2004-2009, Jean-Marc François. All Rights Reserved.
 * Licensed under the New BSD license.  See the LICENSE file.
 */

package hmm;

import java.util.ArrayList;


/**
 * This class can be used to compute the most probable state sequence matching
 * a given observation sequence (given an HMM).
 */

public class ViterbiCalculator
{	
	/*
	 * The psy and delta values, as described in Rabiner and Juand classical
	 * papers.
	 */
		
	private ArrayList<ArrayList<Double>> delta; 
	private ArrayList<ArrayList<Integer>>  psy;
	private HMM hmm;
	private int[] stateSequence;
	private double lnProbability;
	/**
	 * Computes the most likely state sequence matching an observation
	 * sequence given an HMM.
	 *
	 * @param hmm A Hidden Markov Model;
	 */
	
	public ViterbiCalculator(HMM hmm)
	{
		
		this.hmm=hmm;
		
		delta = new ArrayList<ArrayList<Double>>() ;
		psy = new ArrayList<ArrayList<Integer>>();
		stateSequence = new int[hmm.stepNum()];
		
		//Add Probability to first time step
 		delta.add(new ArrayList<Double>());
 		psy.add(new ArrayList<Integer>());
		
		for (int i = 0; i < hmm.getTimestep(0).size(); i++) {
			//Add probability of state (t=0,m=i) to delta matrix 
			delta.get(0).add(new Double(-Math.log(hmm.getObs(0,i).p_emission)
									) 
							);
			psy.get(0).add(0);
		}
		
		for(int t = 1;t < hmm.stepNum();t++){	
			
			for (int i = 0; i < hmm.getTimestep(t).size(); i++)
				computeStep(hmm, t, i);
			
		}
		
		lnProbability = Double.MAX_VALUE;
		//Initialize path for the first 
		for (int i = 0; i < hmm.getTimestep(hmm.stepNum()-1).size(); i++) {
			double thisProbability = delta.get(hmm.stepNum()-1).get(i);
			
			if (lnProbability > thisProbability) {
				lnProbability = thisProbability;
				stateSequence[hmm.stepNum() - 1] = i;
			}
		}
		lnProbability = -lnProbability;
		
		for (int t2 = hmm.stepNum() - 2; t2 >= 0; t2--)
			stateSequence[t2] = psy.get(t2+1).get(stateSequence[t2+1]);
	}
	
	
	/*
	 * Computes delta and psy[t][j] (t > 0) 
	 */
	private void computeStep(HMM hmm, int t, int j) 
	{
		double minDelta = Double.MAX_VALUE;
		int min_psy = 0;
		delta.add(new ArrayList<Double>());
 		psy.add(new ArrayList<Integer>());
		for (int i = 0; i < hmm.getTimestep(t-1).size(); i++) {
			double thisDelta = delta.get(t-1).get(i) - Math.log(hmm.getTransProb(t-1,i, j));
			
			if (minDelta > thisDelta) {
				minDelta = thisDelta;
				min_psy = i;
			}
		}
		
		delta.get(t).add(minDelta - Math.log(hmm.getObs(t, j).p_emission ));
		psy.get(t).add(min_psy);
	}
	
	
	/**
	 * Returns the neperian logarithm of the probability of the given
	 * observation sequence on the most likely state sequence of the given
	 * HMM.
	 *
	 * @return <code>ln(P[O,S|H])</code> where <code>O</code> is the given
	 *         observation sequence, <code>H</code> the given HMM and 
	 *         <code>S</code> the most likely state sequence of this observation
	 *         sequence given this HMM.
	 */
	public double lnProbability()
	{
		return lnProbability;
	}
	
	
	/**
	 * Returns a (clone of) the array containing the computed most likely
	 * state sequence.
	 *
	 * @return The state sequence; the i-th value of the array is the index
	 *         of the i-th state of the state sequence.
	 */
	public Observation[] stateSequence() 
	{
		Observation[] result=new Observation[hmm.stepNum()];
		for(int i=0;i < hmm.stepNum();i++){
			result[i]=hmm.getTimestep(i).get(stateSequence[i]);
		}
		return result;
	}
	
}
