package hmm;
import mapmatch.Config;
import mapmatch.Debugging;

public class EmissionProb {
	private PDF distancePDF;
	private PDF degreePDF;
	private double DEGREE_TO_METER=110648.69;
	
	
	
	public EmissionProb(){
		distancePDF=new Student_TPDF(Config.DISTANCE_MEAN,Config.DISTANCE_VARIENCE,Config.DISTANCE_ALPHA);
		degreePDF=new Student_TPDF(Config.DEGREE_MEAN,Config.DEGREE_VARIENCE,Config.DEGREE_ALPHA);
		
	}
	
	public double getProb(double distance,double degree){
		//Debugging.printlnDebug("Pos ="+distance*DEGREE_TO_METER+" : Heading ="+degree);
		if(Config.AGGREGRATE_METHOD.equals("sum")){		
			return (distancePDF.probability(distance*DEGREE_TO_METER)*Config.DISTANCE_WEIGHT)+
					(degreePDF.probability(degree)*Config.DEGREE_WEIGHT) ;
		}else
			return Math.pow(distancePDF.probability(distance*DEGREE_TO_METER),Config.DISTANCE_WEIGHT)*
					Math.pow(degreePDF.probability(degree),Config.DEGREE_WEIGHT);
	}
	public double getDistanceProb(double distance){
		return distancePDF.probability(distance*DEGREE_TO_METER);
	}
	public double getDegreeProb(double degree){
		return degreePDF.probability(degree);
	}
}
