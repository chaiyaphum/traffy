package hmm;

import java.util.ArrayList;

/*
import org.postgis.LineString;
import org.postgis.Point;
*/

import org.opengis.geometry.coordinate.LineString;
import org.opengis.geometry.primitive.Point;

public class Observation {
	//Define a pair of transition and it probability
	
	public double p_emission;
	private  int linkID;
	private Point p;
	private LineString nsl; //Nearest sub-linestring
	private LineString nl;//Nearest linestring
	public ArrayList<Trans_pair> trans_list;
	public double pos_prob;
	public double heading_prob;
	
	public Observation(double p_e,int alinkID,LineString aNsl,LineString aNl,Point point){
		p_emission = p_e;
		trans_list= new ArrayList<Trans_pair>();
		linkID=alinkID;
		nsl=aNsl;
		nl=aNl;
		p=point;
		
	}
	public Observation(double p_e,int alinkID){
		p_emission = p_e;
		trans_list= new ArrayList<Trans_pair>();
		linkID=alinkID;
		nsl=null;
		nl=null;
		p=null;
	}
	
	public void add_trans(Observation o,double tp){
		trans_list.add(new Trans_pair(o,tp));
	}
	public int getLinkID(){
		return linkID;
	}
	public LineString getNSL(){
		return nsl;
	}
	public Point getPoint(){
		return p;
	}
	public LineString getNL(){
		return nl;
	}
	

}
