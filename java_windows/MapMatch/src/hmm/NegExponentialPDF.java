package hmm;

public class NegExponentialPDF implements PDF{
	public double alpha;
	public double beta;
	
	public NegExponentialPDF(double alpha,double beta){
		this.alpha=alpha;
		this.beta=beta;
	}
	public double probability(double n)
	{
		double expArg = -.5 * (n - alpha) / beta;
		return 1/beta*Math.exp(expArg);
	}
}