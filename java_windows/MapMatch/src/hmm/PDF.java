package hmm;

public interface PDF {
	public double probability(double n);
}
