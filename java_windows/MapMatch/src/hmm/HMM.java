package hmm;

import java.util.ArrayList;

import mapmatch.Debugging;

public class HMM {
	private ArrayList<ArrayList<Observation>> lactice;
	private EmissionProb ep;
	private TransitionProb tp;
	public HMM(){
		lactice=new ArrayList<ArrayList<Observation>>();
		ep =new EmissionProb();
		tp =new TransitionProb();
	}
	public HMM(EmissionProb ep,TransitionProb tp){
		lactice=new ArrayList<ArrayList<Observation>>();
		this.ep =ep;
		this.tp =tp;
	}
	
	public void addTimeStep(ArrayList<Observation> ts){
		ArrayList<Observation> last_timestep=null;
		if(lactice.size() > 0){
			last_timestep=lactice.get(lactice.size()-1);
			for(Observation po : last_timestep){
				// for each observation in last timestep
				for(Observation co :ts){
				// Add the transition to each observation in this time step for them
					po.add_trans(co, TransitionProb.getProb( po.getLinkID(), co.getLinkID() ) );
					
				}
			}
			lactice.add(ts);
		}else
			lactice.add(ts);
	}
	
	public ArrayList<Observation> getTimestep(int idx){
		return lactice.get(idx);
		
	}
	public Observation getObs(int m,int n){
		ArrayList<Observation> timeStep =lactice.get(m);
		if(n > timeStep.size()-1){
			throw new IllegalArgumentException("n Exceed Time step size");
		}else
			return timeStep.get(n);
	}
	
	public Double getTransProb(int i1,int j1,int j2){
		Debugging.printlnDebug("i1 :"+i1+",j1 :"+j1+",j2 :"+j2);
		return this.lactice.get(i1).get(j1).trans_list.get(j2).trans_p;
	}
	
	public int stepNum(){
		return lactice.size();
	}
	public String toString(){
		StringBuffer sb=new StringBuffer();
		for(ArrayList<Observation> ts :lactice){
			sb.append("[");
			for(Observation o : ts){
				sb.append(String.format("Prob : %f ,",o.p_emission) );
				
			}
			sb.append("]\n");
		}
		return sb.toString();
		
	}
}
