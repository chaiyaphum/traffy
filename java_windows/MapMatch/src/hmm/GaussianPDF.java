package hmm;

public class GaussianPDF implements PDF{
	public double mean;
	public double variance;
	public double alpha;
	
	public GaussianPDF(double u,double aVariance,double alpha){
		this.alpha=alpha;
		mean =u;
		variance=aVariance;
	}
	public double probability(double n)
	{
		double expArg = -.5 * (n - mean) * (n - mean) / variance;
		return (alpha*Math.sqrt(1/(Math.PI * variance ) ) ) *
		Math.exp(expArg);
	}
}
