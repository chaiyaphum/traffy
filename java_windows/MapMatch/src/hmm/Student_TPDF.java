package hmm;

public class Student_TPDF implements PDF{
	public double mean;
	public double variance;
	public double alpha;
	
	public Student_TPDF(double u,double aVariance,double alpha){
		this.alpha=alpha;
		mean =u;
		variance=aVariance;
	}
	public double probability(double n)
	{
		return alpha/( Math.PI* (variance + (n-mean)*(n-mean) ) );
	}
}
