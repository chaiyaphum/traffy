package mount;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class M_FindData {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		int countAll_postion = 0;
		Integer[] countPostion = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		Integer[] countCar = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		String[] weekElement = { "gps20130201", "gps20130202", "gps20130203",
				"gps20130204", "gps20130205", "gps20130206", "gps20130207" 
				,"gps20130208", "gps20130209", "gps20130210",
				"gps20130211", "gps20130212", "gps20130213", "gps20130214" 
				,"gps20130215", "gps20130216", "gps20130217",
				"gps20130218", "gps20130219", "gps20130220", "gps20130221" 
				,"gps20130222", "gps20130223", "gps20130224",
				"gps20130225", "gps20130226", "gps20130227", "gps20130228" };

		Double[][] postion = {
				// {lat_min, lat_max, long_min, long_max}
				{ 13.597185, 13.647609, 100.411206, 100.684830 },
				{ 13.625986, 13.640386, 100.411237, 100.425652 },
				{ 13.624549, 13.627902, 100.425652, 100.472507 },
				{ 13.618798, 13.626015, 100.472507, 100.490422 },
				{ 13.618741, 13.622340, 100.490422, 100.512021 },
				{ 13.618802, 13.633219, 100.512021, 100.537237 },
				{ 13.629624, 13.640439, 100.537237, 100.576837 },
				{ 13.625976, 13.633174, 100.576837, 100.594861 },
				{ 13.611514, 13.629597, 100.594861, 100.620064 },
				{ 13.597176, 13.618817, 100.620064, 100.663229 },
				{ 13.618817, 13.647558, 100.659677, 100.684845 } };

		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		PreparedStatement pst2 = null;
		ResultSet rs2 = null;
		PreparedStatement pst3 = null;
		ResultSet rs3 = null;

		String sql_p = null;
		String sql_c = null;
		String sql_cp = null;

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}

		try {
			connection1 = DriverManager.getConnection(url, user, password);
			for (int dayCount = 0; dayCount < 28; dayCount++) {
				for (int i = 0; i < 11; i++) {
					sql_p = "SELECT count(source_id) FROM gps."
							+ weekElement[dayCount] + " where lat between "
							+ postion[i][0] + " and " + postion[i][1]
							+ " and long between " + postion[i][2] + " and "
							+ postion[i][3] + ";";
					sql_c = "SELECT source_id , COUNT(*) AS rows from gps."
							+ weekElement[dayCount] + " where lat between "
							+ postion[i][0] + " and " + postion[i][1]
							+ " and long between " + postion[i][2] + " and "
							+ postion[i][3] + " GROUP BY source_id;";

					pst1 = connection1.prepareStatement(sql_p);
					rs1 = pst1.executeQuery();

					while (rs1.next()) {
						// countPostion.add(rs1.getString("count"));
						countPostion[i] += rs1.getInt("count");
					}

					pst2 = connection1.prepareStatement(sql_c);
					rs2 = pst2.executeQuery();

					int tempCar = 0;
					while (rs2.next()) {
						tempCar++;
					}
					countCar[i] += tempCar;

				}
				sql_cp = "SELECT count(*) FROM gps." + weekElement[dayCount]
						+ ";";
				pst3 = connection1.prepareStatement(sql_cp);
				rs3 = pst3.executeQuery();

				while (rs3.next()) {
					countAll_postion += rs3.getInt("count");
				}
			}

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;

		} finally {
			try {
				rs1.close();
				pst1.close();
				rs2.close();
				pst2.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("=========== Postion ===========");
		for (int i = 0; i < 11; i++) {
			// System.out.println(countPostion.get(i));
			System.out.println(countPostion[i]);
		}
		System.out.println("=========== Car ===========");
		for (int i = 0; i < 11; i++) {
			// System.out.println(countCar.get(i));
			System.out.println(countCar[i]);
		}

		System.out.println("---> " + countAll_postion);
	}

}
