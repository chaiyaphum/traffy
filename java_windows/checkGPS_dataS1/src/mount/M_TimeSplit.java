package mount;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class M_TimeSplit {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		String[] weekElement = { "gps20130201", "gps20130202", "gps20130203",
				"gps20130204", "gps20130205", "gps20130206", "gps20130207",
				"gps20130208", "gps20130209", "gps20130210", "gps20130211",
				"gps20130212", "gps20130213", "gps20130214", "gps20130215",
				"gps20130216", "gps20130217", "gps20130218", "gps20130219",
				"gps20130220", "gps20130221", "gps20130222", "gps20130223",
				"gps20130224", "gps20130225", "gps20130226", "gps20130227",
				"gps20130228" };

		String[] dayElement = { "2013-02-01", "2013-02-02", "2013-02-03",
				"2013-02-04", "2013-02-05", "2013-02-06", "2013-02-07",
				"2013-02-08", "2013-02-09", "2013-02-10", "2013-02-11",
				"2013-02-12", "2013-02-13", "2013-02-14", "2013-02-15",
				"2013-02-16", "2013-02-17", "2013-02-18", "2013-02-19",
				"2013-02-20", "2013-02-21", "2013-02-22", "2013-02-23",
				"2013-02-24", "2013-02-25", "2013-02-26", "2013-02-27",
				"2013-02-28" };

		int[][] countSumP = new int[11][24];
		// Arrays.fill(countSumP, 0);
		for (int x = 0; x < countSumP.length; x++) {
			for (int y = 0; y < countSumP[x].length; y++) {
				countSumP[x][y] = 0;
			}
		}

		Double[][] postion = {
				// {lat_min, lat_max, long_min, long_max}
				{ 13.597185, 13.647609, 100.411206, 100.684830 },
				{ 13.625986, 13.640386, 100.411237, 100.425652 },
				{ 13.624549, 13.627902, 100.425652, 100.472507 },
				{ 13.618798, 13.626015, 100.472507, 100.490422 },
				{ 13.618741, 13.622340, 100.490422, 100.512021 },
				{ 13.618802, 13.633219, 100.512021, 100.537237 },
				{ 13.629624, 13.640439, 100.537237, 100.576837 },
				{ 13.625976, 13.633174, 100.576837, 100.594861 },
				{ 13.611514, 13.629597, 100.594861, 100.620064 },
				{ 13.597176, 13.618817, 100.620064, 100.663229 },
				{ 13.618817, 13.647558, 100.659677, 100.684845 } };

		String[] time = { "00", "01", "02", "03", "04", "05", "06", "07", "08",
				"09", "10", "11", "12", "13", "14", "15", "16", "17", "18",
				"19", "20", "21", "22", "23", "24" };

		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;

		String sql_p = null;

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			for (int dayCount = 0; dayCount < 28; dayCount++) {
				System.out.println("Day : " + dayCount);
				for (int i = 0; i < postion.length; i++) {
					//System.out.println("Area : " + i);
					for (int t = 0; t < 24; t++) {
						// System.out.println("time : " + time[t] + " to " +

						if (i == 0) {
							sql_p = "select count(*) from gps."
									+ weekElement[dayCount] + " where (";
							for (int j = 1; j < postion.length; j++) {
								sql_p += "(lat between " + postion[i][0]
										+ " and " + postion[j][1]
										+ " and long between " + postion[j][2]
										+ " and " + postion[j][3] + ") or ";
							}
							sql_p += "false)";
						} else {
							sql_p = "select count(*) from gps."
									+ weekElement[dayCount]
									+ " where (lat between " + postion[i][0]
									+ " and " + postion[i][1]
									+ " and long between " + postion[i][2]
									+ " and " + postion[i][3] + ")";
						}

						sql_p += " and date_time >= '" + dayElement[dayCount]
								+ " " + time[t] + ":00:00' and date_time <= '"
								+ dayElement[dayCount] + " " + time[t + 1]
								+ ":00:00'";

						// System.out.println(sql_p);

						pst1 = connection1.prepareStatement(sql_p);
						rs1 = pst1.executeQuery();

						while (rs1.next()) {
							// System.out.println(rs1.getString("count"));
							countSumP[i][t] += rs1.getInt("count");
						}
					}
				}
			}

			for (int x = 0; x < countSumP.length; x++) {
				System.out.println("A " + x);
				for (int y = 0; y < countSumP[x].length; y++) {
					System.out.println(countSumP[x][y]);
				}
			}

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;

		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
