package traffic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CountSpeed {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;

		String[] sql = new String[2];
		String[] day = { "gps20130304", "gps20130305", "gps20130306",
				"gps20130307", "gps20130308", "gps20130309", "gps20130310" };
		String s;

		String[] speed_store = new String[151];

		for (int i = 0; i <= 150; i++) {
			speed_store[i] = String.valueOf(i);
		}

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}

		try {
			connection1 = DriverManager.getConnection(url, user, password);
			for (int i = 0; i < 2; i++) {
				int[] averageSpeed = new int[151];
				
				for (int dayLoop = 0; dayLoop < 7; dayLoop++) {

					sql[0] = "SELECT speed , COUNT(*) AS rows FROM gps."
							+ day[dayLoop]
							+ " where (lat>=13.618799 and lat<=13.645942 and long>=100.661859 and long<=100.685066) and ( heading between 10 and 80) GROUP BY speed;";
					sql[1] = "SELECT speed , COUNT(*) AS rows FROM gps."
							+ day[dayLoop]
							+ " where (lat>=13.618799 and lat<=13.645942 and long>=100.661859 and long<=100.685066) and (heading between 190 and 260) GROUP BY speed;";
					// System.out.println("\n====== " + day[dayLoop] +
					// " ======\n");

					pst1 = connection1.prepareStatement(sql[i]);
					rs1 = pst1.executeQuery();

					while (rs1.next()) {
						s = rs1.getString("speed");
						// System.out.println(Arrays.asList(speed_store).indexOf(s));
						averageSpeed[Arrays.asList(speed_store).indexOf(s)] += rs1
								.getInt("rows");
					}
				}

				for (int k = 0; k < 151; k++) {
					System.out.println(k + "\t" + averageSpeed[k]);
				}
				System.out.println("\n\n");

			}

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;

		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
