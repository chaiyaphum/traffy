package traffic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class FindTimeAverage {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;

		String[] sql = new String[2];
		String[] h_store = { "00", "01", "02", "03", "04", "05", "06", "07",
				"08", "09", "10", "11", "12", "13", "14", "15", "16", "17",
				"18", "19", "20", "21", "22", "23" };
		String[] day = { "gps20130204", "gps20130205", "gps20130206",
				"gps20130207", "gps20130208", "gps20130209", "gps20130210" };
		String h;

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			for (int dayLoop = 0; dayLoop < 7; dayLoop++) {
				int[] averageSpeed = new int[24];
				int[] countCar = new int[24];
				Arrays.fill(averageSpeed, 0);
				Arrays.fill(countCar, 0);
				sql[0] = "SELECT *FROM gps." + day[dayLoop] + " where ((lat>=13.632942 and lat<=13.639048 and long>=100.557565 and long<=100.57085) or (lat>=113.630234 and lat<=13.633827 and long>=100.57085 and long<=100.585013)) and (heading between 90 and 150) order by date_time asc;";
				sql[1] = "SELECT *FROM gps." + day[dayLoop] + " where ((lat>=13.632942 and lat<=13.639048 and long>=100.557565 and long<=100.57085) or (lat>=113.630234 and lat<=13.633827 and long>=100.57085 and long<=100.585013)) and (heading between 270 and 330) order by date_time asc;";
				
				for (int i = 0; i < 2; i++) {
					pst1 = connection1.prepareStatement(sql[i]);
					rs1 = pst1.executeQuery();
					//System.out.println("\n====== " + i + " ======\n");

					while (rs1.next()) {
						h = rs1.getString("date_time").substring(11, 13);
						averageSpeed[Arrays.asList(h_store).indexOf(h)] += rs1
								.getDouble("speed");
						countCar[Arrays.asList(h_store).indexOf(h)]++;
					}

					for (int k = 0; k < 24; k++) {
						if (countCar[k] == 0) {
							System.out.print(0 + "\t");
						} else {
							System.out.print(averageSpeed[k] / countCar[k]
									+ "\t");
						}

					}
				}
			}

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;

		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
