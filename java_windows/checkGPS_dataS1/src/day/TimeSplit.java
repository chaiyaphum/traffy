package day;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class TimeSplit {
	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		Double[][] postion = {
				// {lat_min, lat_max, long_min, long_max}
				{ 13.597185, 13.647609, 100.411206, 100.684830 },
				{ 13.625986, 13.640386, 100.411237, 100.425652 },
				{ 13.624549, 13.627902, 100.425652, 100.472507 },
				{ 13.618798, 13.626015, 100.472507, 100.490422 },
				{ 13.618741, 13.622340, 100.490422, 100.512021 },
				{ 13.618802, 13.633219, 100.512021, 100.537237 },
				{ 13.629624, 13.640439, 100.537237, 100.576837 },
				{ 13.625976, 13.633174, 100.576837, 100.594861 },
				{ 13.611514, 13.629597, 100.594861, 100.620064 },
				{ 13.597176, 13.618817, 100.620064, 100.663229 },
				{ 13.618817, 13.647558, 100.659677, 100.684845 } };

		String[] time = { "00", "01", "02", "03", "04", "05", "06", "07", "08",
				"09", "10", "11", "12", "13", "14", "15", "16", "17", "18",
				"19", "20", "21", "22", "23", "24" };

		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;

		String sql_p = null;

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			for (int i = 0; i < postion.length; i++) {
				System.out.println("Area : " + i);
				for (int t = 0; t < 24; t++) {
					// System.out.println("time : " + time[t] + " to " +

					if (i == 0) {
						sql_p = "select count(*) from gps.gps20130314 where (";
						for (int j = 1; j < postion.length; j++) {
							sql_p += "(lat between " + postion[i][0] + " and "
									+ postion[j][1] + " and long between "
									+ postion[j][2] + " and " + postion[j][3]
									+ ") or ";
						}
						sql_p += "false)";
					} else {
						sql_p = "select count(*) from gps.gps20130314_f where (lat between "
								+ postion[i][0]
								+ " and "
								+ postion[i][1]
								+ " and long between "
								+ postion[i][2]
								+ " and " + postion[i][3] + ")";
					}
					sql_p += " and date_time >= '2013-03-14 " + time[t]
							+ ":00:00' and date_time <= '2013-03-14 "
							+ time[t + 1] + ":00:00'";

					// System.out.println(sql_p);

					pst1 = connection1.prepareStatement(sql_p);
					rs1 = pst1.executeQuery();

					while (rs1.next()) {
						System.out.println(rs1.getString("count"));
					}
					// System.out.println("==================================");
				}
			}

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;

		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
