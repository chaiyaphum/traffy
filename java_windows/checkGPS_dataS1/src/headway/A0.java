package headway;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class A0 {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;

		String sql, sourceId_read, dateTime_read;
		int average = 0, diff_time, h1, m1, s1, h2, m2, s2;
		Double lat_read, long_read;
		int current_index;

		ArrayList<String> sourceId_store = new ArrayList<String>();
		ArrayList<Double> long_store = new ArrayList<Double>();

		// Probe header data
		ArrayList<String> probeHeadwayDate = new ArrayList<String>();

		// headwayLocation[lat_min][long_min] [lat_max][long_max]
		Double[] headwayLocation = { 13.624616, 100.425696, 13.627902,
				100.472500 };
		Double[] locationArea2 = { 13.618798, 100.472507, 13.626015, 100.490422 };

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			sql = "SELECT * FROM gps.gps20130314 where heading between 0 and 180 order by date_time asc;";
			pst1 = connection1.prepareStatement(sql);
			rs1 = pst1.executeQuery();

			while (rs1.next()) {
				sourceId_read = rs1.getString("source_id");
				long_read = rs1.getDouble("long");
				lat_read = rs1.getDouble("lat");
				dateTime_read = rs1.getString("date_time");

				if (sourceId_store.contains(sourceId_read)) {
					current_index = sourceId_store.indexOf(sourceId_read);

					// Check probe pass observation
					if ((long_read > headwayLocation[1] && long_read <= locationArea2[3])
							&& ((lat_read >= headwayLocation[0] && lat_read <= headwayLocation[2]) || (lat_read >= locationArea2[0] && lat_read <= locationArea2[2]))) {

						sourceId_store.remove(current_index);
						long_store.remove(current_index);

						probeHeadwayDate.add(dateTime_read);
						// System.out.println(sourceId_read + " " +
						// dateTime_read + " " + lat_read + "," + long_read);
						continue;
					}

					// probe before observation
					if (long_read <= headwayLocation[1]
							&& long_read >= long_store.get(current_index)) {
						long_store.add(current_index, long_read);
					}

				} else {
					// No source_id in array list -> add source_id , longitude
					if (long_read <= headwayLocation[1]) {
						sourceId_store.add(sourceId_read);
						long_store.add(long_read);
					}
				}
			}

			for (int i = 0; i < probeHeadwayDate.size() - 1; i++) {
				h1 = Integer
						.parseInt(probeHeadwayDate.get(i).substring(11, 13));
				m1 = Integer
						.parseInt(probeHeadwayDate.get(i).substring(14, 16));
				s1 = Integer
						.parseInt(probeHeadwayDate.get(i).substring(17, 19));
				h2 = Integer.parseInt(probeHeadwayDate.get(i + 1).substring(11,
						13));
				m2 = Integer.parseInt(probeHeadwayDate.get(i + 1).substring(14,
						16));
				s2 = Integer.parseInt(probeHeadwayDate.get(i + 1).substring(17,
						19));

				diff_time = (h2 - h1) * 3600 + (m2 - m1) * 60 + (s2 - s1);
				average += diff_time;
				
				System.out.println(diff_time + " => " +  diff_time/60 + "m " + diff_time%60 + "s");
			}
			
			average = average/probeHeadwayDate.size();
			System.out.println("\naverage = " + average + " => " +  average/60 + "m " + average%60 + "s");

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;
		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}