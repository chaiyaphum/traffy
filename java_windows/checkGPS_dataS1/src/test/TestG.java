package test;

import java.text.SimpleDateFormat;

public class TestG {

	public static void main(String[] args) {
		String LINK_SCHEMA = "road_data";
		String LINK_TABLE = "roadmot_expway";
		String searchFunction = "find_link";
		Double lat = 0.0;
		Double lng = 0.0;
		String rectime = "2012-12-14 00:00:00";

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		String sql = String.format("select gid,oneway,the_geom " + "from %s."
				+ searchFunction + "('%s.%s',%f,%f) ;", LINK_SCHEMA,
				LINK_SCHEMA, LINK_TABLE, lng, lat);

		System.out.println(sql);
	}

}
