package week;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class W_FindTime {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		ArrayList<String> sourceId_store = new ArrayList<String>();
		// ArrayList<Integer> countMax = new ArrayList<Integer>();
		// ArrayList<Integer> countAvg = new ArrayList<Integer>();

		int[][] countMax = new int[7][11];
		int[][] countAvg = new int[7][11];
		
		int[] countMax_temp = new int[11];
		int countAvg_temp;

		String[] weekElement = { "gps20130308", "gps20130309", "gps20130310",
				"gps20130311", "gps20130312", "gps20130313", "gps20130314" };
		Double[][] postion = {
				// {lat_min, lat_max, long_min, long_max}
				{ 13.597185, 13.647609, 100.411206, 100.684830 },
				{ 13.625986, 13.640386, 100.411237, 100.425652 },
				{ 13.624549, 13.627902, 100.425652, 100.472507 },
				{ 13.618798, 13.626015, 100.472507, 100.490422 },
				{ 13.618741, 13.622340, 100.490422, 100.512021 },
				{ 13.618802, 13.633219, 100.512021, 100.537237 },
				{ 13.629624, 13.640439, 100.537237, 100.576837 },
				{ 13.625976, 13.633174, 100.576837, 100.594861 },
				{ 13.611514, 13.629597, 100.594861, 100.620064 },
				{ 13.597176, 13.618817, 100.620064, 100.663229 },
				{ 13.618817, 13.647558, 100.659677, 100.684845 } };

		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;

		String sql_p = null;
		String sourceTemp = null;
		int h_store = 0, m_store = 0, s_store = 0, h, m, s, diff_time;
		int min = -1, max = 0, avg = 0, count = 0;

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			for (int dayCount = 0; dayCount < 7; dayCount++) {
				// System.out.println("--> " + weekElement[dayCount]);
				for (int i = 0; i < postion.length; i++) {
					count = 0;
					max = 0;
					// System.out.println("Area : " + i);

					if (i == 0) {
						sql_p = "select * from gps." + weekElement[dayCount]
								+ " where";
						for (int j = 1; j < postion.length; j++) {
							sql_p += " (lat between " + postion[j][0] + " and "
									+ postion[j][1] + " and long between "
									+ postion[j][2] + " and " + postion[j][3]
									+ ") or";
						}
						sql_p += " false order by date_time ,source_id asc";
					} else {
						sql_p = "select * from gps." + weekElement[dayCount]
								+ " where lat between " + postion[i][0]
								+ " and " + postion[i][1]
								+ " and long between " + postion[i][2]
								+ " and " + postion[i][3]
								+ " order by date_time ,source_id asc";
					}

					// System.out.println(sql_p);

					sourceTemp = null;
					sourceId_store.clear();
					pst1 = connection1.prepareStatement(sql_p);
					rs1 = pst1.executeQuery();

					while (rs1.next()) {
						if (sourceTemp == null) {
							sourceTemp = rs1.getString("source_id");
							h_store = Integer.parseInt(rs1.getString(
									"date_time").substring(11, 13));
							m_store = Integer.parseInt(rs1.getString(
									"date_time").substring(14, 16));
							s_store = Integer.parseInt(rs1.getString(
									"date_time").substring(17, 19));

							sourceId_store.add(sourceTemp);
							continue;
						}

						if (!(sourceId_store.contains(rs1
								.getString("source_id")))) {
							h = Integer.parseInt(rs1.getString("date_time")
									.substring(11, 13));
							m = Integer.parseInt(rs1.getString("date_time")
									.substring(14, 16));
							s = Integer.parseInt(rs1.getString("date_time")
									.substring(17, 19));

							diff_time = (h - h_store) * 3600 + (m - m_store)
									* 60 + (m - m_store);

							if (min == -1) {
								min = diff_time;
								max = diff_time;
							} else {
								if (min > diff_time) {
									min = diff_time;
								} else if (max < diff_time) {
									max = diff_time;
								}
							}

							sourceTemp = rs1.getString("source_id");
							sourceId_store.add(sourceTemp);

							h_store = Integer.parseInt(rs1.getString(
									"date_time").substring(11, 13));
							m_store = Integer.parseInt(rs1.getString(
									"date_time").substring(14, 16));
							s_store = Integer.parseInt(rs1.getString(
									"date_time").substring(17, 19));

							avg += diff_time;
							count++;
						}
					}

					avg = avg / count;

					countMax[dayCount][i] = max;
					countAvg[dayCount][i] = avg;

					/*
					 * System.out.println("min = " + min + " : max = " + max +
					 * " : Average = " + avg);
					 * 
					 * System.out.println("min = " + min / 60 + "." + min % 60 +
					 * " : max = " + max / 60 + "." + max % 60 + " : Average = "
					 * + avg / 60 + "." + avg % 60);
					 * System.out.println("==================================");
					 */
					// min, max, average
					// System.out.println(min + "\t" + max
					// + "\t" + avg);

				}
			}

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;

		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < 11; i++) {
			countAvg_temp = 0;
			for (int j = 0; j < 7; j++) {
				//System.out.print(countMax[j][i] + " ");
				countMax_temp[j] = countMax[j][i];
				countAvg_temp += countAvg[j][i]; 
			}
			Arrays.sort(countMax_temp);
			System.out.println(countMax_temp[countMax_temp.length - 1] + "\t" +  countAvg_temp/7.0);
		}
		
	}
}
