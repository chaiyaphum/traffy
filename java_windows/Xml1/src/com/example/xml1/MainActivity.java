package com.example.xml1;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.AssetManager;
import android.util.Log;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		parseXML();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void parseXML() {
		AssetManager assetManager = getBaseContext().getAssets();
		try {
			InputStream is = assetManager.open("order.xml");
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			OrderXMLHandler myXMLHandler = new OrderXMLHandler();
			xr.setContentHandler(myXMLHandler);
			InputSource inStream = new InputSource(is);
			xr.parse(inStream);

			String cartId = myXMLHandler.getCartId();
			String customerId = myXMLHandler.getCustomerId();
			String email = myXMLHandler.getEmail();

			Log.v("abc", cartId);
			LinearLayout ll = (LinearLayout) findViewById(R.id.linearLayout1);
			TextView tv = new TextView(this);
			tv.setText("Cart Id: " + cartId);
			ll.addView(tv);
			tv = new TextView(this);
			tv.setText("Customer Id: " + customerId);
			ll.addView(tv);
			tv = new TextView(this);
			tv.setText("Email : " + email);
			ll.addView(tv);
			tv = new TextView(this);
			tv.setText("Shopping Cart Info --->");
			ll.addView(tv);

			ArrayList<ProductInfo> cartList = myXMLHandler.getCartList();
			for (ProductInfo productInfo : cartList) {
				tv = new TextView(this);
				tv.setText("Line No : " + productInfo.getSeqNo());
				ll.addView(tv);
				tv = new TextView(this);
				tv.setText("Item No : " + productInfo.getItemNumber());
				ll.addView(tv);
				tv = new TextView(this);
				tv.setText("Quantity : " + productInfo.getQuantity());
				ll.addView(tv);
				tv = new TextView(this);
				tv.setText("Price : " + productInfo.getPrice());
				ll.addView(tv);
				tv = new TextView(this);
				tv.setText("---");
				ll.addView(tv);
			}

			is.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
