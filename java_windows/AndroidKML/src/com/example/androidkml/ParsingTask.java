package com.example.androidkml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.example.simpleKML.Serializer;
import com.example.simpleKML.model.Kml;

public class ParsingTask extends AsyncTask<String, String, Kml> {
	private final String TAG = "MainActivity";
	Serializer kmlSerializer;

	public ParsingTask() {
		kmlSerializer = new Serializer();
	}

	@Override
	protected Kml doInBackground(String... arg0) {
		
		Log.d(TAG, "read started");

		Kml kml = null;
		try {
			InputStream is = getResources().getAssets().open(params[0]);
			Log.d(TAG, "parsing started");
			kml = kmlSerializer.read(is);
			Log.d(TAG, "parsing done");
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		Log.d(TAG, "read done");

		if (kml != null) {
			Log.d(TAG, "write started");
			// this will output the KML to
			// /data/data/com.ekito.simplekmldemo/example_out.kml
			File out = new File(getDir("assets", Context.MODE_PRIVATE),
					"test.kml");
			try {
				kmlSerializer.write(kml, out);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			Log.d(TAG, "write done");
		}

		return kml;
	}

}
