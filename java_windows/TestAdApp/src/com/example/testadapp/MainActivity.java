package com.example.testadapp;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import android.widget.TextView;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

public class MainActivity extends Activity {
	private TextView output;
	private LocationManager lm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		output = (TextView) findViewById(R.id.textView1);
		lm = (LocationManager) getSystemService(LOCATION_SERVICE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 1, (LocationListener) this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		lm.removeUpdates((LocationListener) this);
	}
	
	public void onLocationChanged(Location loc) {
		if(loc == null)
			output.append("Cat find location\n");
		else {
			output.append("lat = " + loc.getLatitude());
			output.append("lat = " + loc.getLongitude() + "\n");
		}
	}
	
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
