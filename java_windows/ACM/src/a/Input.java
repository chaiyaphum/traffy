package a;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

// Put txt file at root folder
public class Input {

	public static void main(String[] args) throws FileNotFoundException {
		//Scanner scan = new Scanner(System.in);
		Scanner scan = new Scanner(new File("f1.txt"));
		
		int num1 = scan.nextInt();
		System.out.print(num1);
		
		scan.close();
	}

}
