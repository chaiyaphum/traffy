package uva;

import java.util.Scanner;

/* (A*B*C) mod N == ((A mod N) * (B mod N) * (C mod N)) mod N.  */

public class BigMod_374 {

	static long bigmod(long b, long p, long m) {
		if (p == 0)
			return 1;
		else if (p % 2 == 0) {
			return (long) (Math.pow(bigmod(b, p / 2, m), 2) % m);
		} else {
			return ((b % m) * bigmod(b, p - 1, m)) % m;
		}
	}

	public static void main(String[] args) {
		long b, p, m;
		Scanner in = new Scanner(System.in);

		while (in.hasNextLong()) {
			b = in.nextLong();
			p = in.nextLong();
			m = in.nextLong();

			System.out.println(bigmod(b, p, m));
		}

	}
}
