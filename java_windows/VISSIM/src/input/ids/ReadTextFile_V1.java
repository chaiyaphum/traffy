package input.ids;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class ReadTextFile_V1 {

	public static void main(String[] args) throws IOException {
		InputStream fis;
		BufferedReader br;
		String line;

		ArrayList<String> fileName = new ArrayList<String>();
		File[] files = new File("input").listFiles();

		for (File file : files) {
			if (file.isFile()) {
				fileName.add(file.getName());
			}
		}

		for (int i = 0; i < fileName.size(); i++) {
			fis = new FileInputStream("input/" + fileName.get(i));
			br = new BufferedReader(new InputStreamReader(fis,
					Charset.forName("UTF-8")));

			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
					"output/" + fileName.get(i)), true));

			while ((line = br.readLine()) != null) {

				if (!line.contains("State Change")) {
					bw.write(line);
					bw.newLine();
				}
			}
			
			System.out.println("Done -> " + fileName.get(i));
			br.close();
			bw.close();
			br = null;
			fis = null;
		}

	}

}
