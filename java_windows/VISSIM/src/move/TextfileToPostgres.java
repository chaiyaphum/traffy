package move;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.io.*;
import java.util.*;

public class TextfileToPostgres {

	public static void main(String[] args) throws IOException {
		InputStream fis;
		BufferedReader br;
		String dir = "data/";
		String line;

		Connection con = null;
		PreparedStatement pst = null;
		String url = "jdbc:postgresql://localhost:5433/traffic_camara";
		String user = "chaiyaphum";
		String password = "14112534x";

		String[] fileName = { "20120503_1A.txt", "20120503_1B.txt",
				"20120503_2A.txt", "20120503_2B.txt", "20120503_3A.txt",
				"20120503_3B.txt", "20120503_4A.txt", "20120503_4B.txt",
				"20120503_5A.txt", "20120503_5B.txt", "20120503_6A.txt",
				"20120503_6B.txt", "20120503_7A.txt", "20120503_7B.txt",
				"20120503_8A.txt", "20120503_8B.txt", "20120503_9A.txt",
				"20120503_9B.txt", "20120503_10A.txt", "20120503_10B.txt",
				"20120503_11A.txt", "20120503_11B.txt", "20120503_12A.txt",
				"20120503_12B.txt" };

		String[] dbName = { "1a", "1b", "2a", "2b", "3a", "3b", "4a", "4b",
				"5a", "5b", "6a", "6b", "7a", "7b", "8a", "8b", "9a", "9b",
				"10a", "10b", "11a", "11b", "12a", "12b" };
		for (int i = 0; i < fileName.length; i++) {
		//for (int i = 0; i < 1; i++) {

			try {
				con = DriverManager.getConnection(url, user, password);

				String[] main_seg;
				String sqlStatemen;
				fis = new FileInputStream(dir + fileName[i]);
				br = new BufferedReader(new InputStreamReader(fis,
						Charset.forName("UTF-8")));

				ArrayList<String[]> data_row = new ArrayList<String[]>();

				int count = 0;
				while ((line = br.readLine()) != null) {
					String[] traffic = new String[17];
					main_seg = line.split(";");

					if (main_seg.length == 27) {
						traffic[0] = main_seg[3];
						traffic[1] = main_seg[5];
						traffic[2] = main_seg[7];
						traffic[3] = main_seg[13];
						traffic[4] = main_seg[14];
						traffic[5] = main_seg[16];
						traffic[6] = main_seg[17];
						traffic[7] = main_seg[18];
						traffic[8] = main_seg[19];
						traffic[9] = main_seg[20];
						traffic[10] = main_seg[21];
						traffic[11] = main_seg[15];
						traffic[12] = main_seg[22];
						traffic[13] = main_seg[23];
						traffic[14] = main_seg[24];
						traffic[15] = main_seg[25];
						traffic[16] = main_seg[26];

						data_row.add(traffic);
					}
				}

				for (int j = 0; j < data_row.size(); j++) {
					// for (int j = 0; j < 1; j++) {
					String[] tempData = data_row.get(j);

					sqlStatemen = "INSERT INTO \"20120503\".\""
							+ dbName[i]
							+ "\"(detector_title, \"time\", data_interval, average_flow_rate, volume, \"class_A\", \"class_B\", \"class_C\", \"class_D\", \"class_E\", average_time_headway, arithmetic_mean_speed, average_time_occupancy, level_of_service, space_mean_speed, space_occupancy, density) VALUES (\'"
							+ tempData[0] + "\', \'" + tempData[1] + "\', \'"
							+ tempData[2] + "\', " + tempData[3] + ", "
							+ tempData[4] + ", " + tempData[5] + ", "
							+ tempData[6] + ", " + tempData[7] + ", "
							+ tempData[8] + ", " + tempData[9] + ", "
							+ tempData[10] + ", " + tempData[11] + ", "
							+ tempData[12] + ", \'" + tempData[13] + "\', "
							+ tempData[14] + ", " + tempData[15] + ", "
							+ tempData[16] + ");";

					if (tempData[2].contains("Minute")) {
						count++;
						pst = con.prepareStatement(sqlStatemen);
						pst.executeUpdate();
					}
				}

				System.out.println("OK : " + dbName[i] + " -> " + count
						+ " rows");

			} catch (SQLException ex) {
				System.out.println("Connection Failed!");
				return;
			} finally {
				try {
					if (pst != null) {
						pst.close();
					}
					if (con != null) {
						con.close();
					}
				} catch (SQLException ex) {
					return;
				}
			}

			br.close();
			br = null;
			fis = null;
		}
	}
}