package filter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.print.attribute.standard.Media;

public class splitSegmentTest_V1 {

	public static void main(String[] args) throws IOException {
		InputStream fis;
		BufferedReader br;
		String dir = "input_15m/";
		String line;

		ArrayList<String> lane1 = new ArrayList<String>();
		ArrayList<String> lane2 = new ArrayList<String>();
		ArrayList<String> total = new ArrayList<String>();

		File[] files = new File(dir).listFiles();
		ArrayList<String> fileName = new ArrayList<String>();

		for (File file : files) {
			if (file.isFile()) {
				fileName.add(file.getName());
			}
		}

		for (int i = 0; i < fileName.size(); i++) {
			// for (int i = 0; i < 1; i++) {
			fis = new FileInputStream(dir + fileName.get(i));
			br = new BufferedReader(new InputStreamReader(fis,
					Charset.forName("UTF-8")));

			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
					"output_averageHour/" + fileName.get(i)), true));

			lane1.clear();
			lane2.clear();
			total.clear();

			while ((line = br.readLine()) != null) {
				if (line.contains("CPU Identifier")) {
					continue;
				}

				if (line.split(";")[3].equals("Lane 1")) {
					lane1.add(line);
				} else if (line.split(";")[3].equals("Lane 2")) {
					lane2.add(line);
				} else if (line.split(";")[3].equals("Total")) {
					total.add(line);
				}
			}

			ProcessSegment(bw, lane1);
			ProcessSegment(bw, lane2);
			ProcessSegment(bw, total);

			br.close();
			br = null;
			fis = null;
			bw.close();
		}
	}

	static void ProcessSegment(BufferedWriter bw, ArrayList<String> segmentList)
			throws IOException {
		Double[] trafficData = new Double[13];

		int countRound = 1;
		String line;
		String[] main_seg;

		for (int i = 0; i < trafficData.length; i++) {
			trafficData[i] = 0.0;
		}

		while (countRound <= segmentList.size()) {
			line = segmentList.get(countRound - 1);
			main_seg = line.split(";");

			trafficData[0] += Integer.parseInt(main_seg[13]);
			trafficData[1] += Integer.parseInt(main_seg[14]);
			trafficData[2] += Double.parseDouble(main_seg[15]);

			trafficData[3] += Integer.parseInt(main_seg[16]);
			trafficData[4] += Integer.parseInt(main_seg[17]);
			trafficData[5] += Integer.parseInt(main_seg[18]);
			trafficData[6] += Integer.parseInt(main_seg[19]);
			trafficData[7] += Integer.parseInt(main_seg[20]);

			trafficData[8] += Double.parseDouble(main_seg[21]);
			trafficData[9] += Double.parseDouble(main_seg[22]);
			trafficData[10] += Double.parseDouble(main_seg[24]);
			trafficData[11] += Double.parseDouble(main_seg[25]);
			trafficData[12] += Double.parseDouble(main_seg[26]);

			if (countRound % 4 == 0) {
				// Reset variable for each round
				for (int i = 0; i < trafficData.length; i++) {
					trafficData[i] = trafficData[i] / 4;
					bw.write(trafficData[i] + "\t");
					trafficData[i] = 0.0;
				}
				bw.newLine();
			}
			countRound++;
		}
		bw.newLine();
	}

}