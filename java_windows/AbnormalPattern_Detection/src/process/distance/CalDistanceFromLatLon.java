package process.distance;

public class CalDistanceFromLatLon {

	static double R = 6371; // Radius of the earth in km

	public static void main(String[] args) {
		// Location 1 = 13.627148 100.42609
		// Location 2 = 13.626397 100.44735
		
		Double lat1,lat2,long1,long2;
		
		lat1 = 13.627148;
		long1 = 100.42609;
		lat2 = 13.626397;
		long2 = 100.44735;
		
		//System.out.println(getDistanceInKm(lat1, long1, lat2, long2));	
		System.out.println((getDistanceInKm(lat1, long1, lat2, long2)) * 1000);
	}

	public static double getDistanceInKm(double lat1, double long1,
			double lat2, double long2) {

		double dLat, dLong, a, c, distance;

		dLat = deg2rad(lat2 - lat1);
		dLong = deg2rad(long2 - long1);

		a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1))
				* Math.cos(deg2rad(lat2)) * Math.sin(dLong / 2)
				* Math.sin(dLong / 2);

		c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		distance = R * c; // Distance in km
		
		return distance;
	}

	public static double deg2rad(double deg) {
		return deg * (Math.PI / 180);
	}
}