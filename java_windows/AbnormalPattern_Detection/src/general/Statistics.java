package general;

import java.util.ArrayList;

public class Statistics {
	ArrayList<Double> data;
	int size;
	
	public Statistics() {}

	public Statistics(ArrayList<Double> data) {
		this.data = data;
		size = data.size();
	}
	
	public void setData(ArrayList<Double> dataList) {
		data.clear();
		data.addAll(dataList);
	}
	
	public void addValue(double value) {
		data.add(value);
	}

	public double getMean() {
		double sum = 0.0;
		for (double value : data)
			sum += value;
		return sum / size;
	}

	public double getVariance() {
		double mean = getMean();
		double temp = 0;
		for (double value : data)
			temp += (mean - value) * (mean - value);
		return temp / size;
	}

	public double getStdDev() {
		return Math.sqrt(getVariance());
	}
}
