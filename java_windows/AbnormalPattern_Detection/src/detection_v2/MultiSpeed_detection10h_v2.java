package detection_v2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class MultiSpeed_detection10h_v2 {

	public static void main(String[] args) throws SQLException {
		int WINDOW_SIZE = 6;
		
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "chaiyaphum";
		String password = "14112534x";
		String speedTable_name = "gen.\"incident_10h\"";
		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		String sqlStatemen;
		
		HashMap<String, Integer> sourceId_mapping = new HashMap<String, Integer>();
		
		// multiwindow detection variable
		int sourceId_index = 0;
		int currentIndex_mapping;

		// detection variable
		ArrayList<ArrayList<Double>> diffMean = new ArrayList<ArrayList<Double>>();

		// all viriable
		ArrayList<ArrayList<String>> allRef_time = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<Double>> all_sum = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Double>> all_mean = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<String>> all_sumDistance = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> all_speedKm = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<Double>> all_speedInM = new ArrayList<ArrayList<Double>>();

		// window variable
		ArrayList<ArrayList<Double>> window_mean = new ArrayList<ArrayList<Double>>();
		
		// database variable
		String time_db;
		String sourceId_db;
		String sumDistance_db;
		String speed_km;
		
		double[] window_sum = new double[WINDOW_SIZE];
		int sub_index = 0;
		double currentSpeed;

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			sqlStatemen = "SELECT * FROM " + speedTable_name + " order by source_time";

			pst1 = connection1.prepareStatement(sqlStatemen);
			rs1 = pst1.executeQuery();
			
			while (rs1.next()) {
				time_db = rs1.getString("source_time");
				sourceId_db = rs1.getString("id");
				currentSpeed = Double.parseDouble(rs1.getString("speed"));
				speed_km = rs1.getString("speed_km");
				sumDistance_db = rs1.getString("sum_distance");

				if(!sourceId_mapping.containsKey(sourceId_db)) {
					sourceId_mapping.put(sourceId_db, sourceId_index);
					diffMean.add(new ArrayList<Double>());
					all_sum.add(new ArrayList<Double>());
					all_mean.add(new ArrayList<Double>());
					allRef_time.add(new ArrayList<String>());
					window_mean.add(new ArrayList<Double>());
					all_sumDistance.add(new ArrayList<String>());
					all_speedKm.add(new ArrayList<String>());
					all_speedInM.add(new ArrayList<Double>());
					
					sourceId_index++;
				}
				
				currentIndex_mapping = sourceId_mapping.get(sourceId_db);

				all_sum.get(currentIndex_mapping).add(currentSpeed);
				all_mean.get(currentIndex_mapping).add(getMean(all_sum.get(currentIndex_mapping)));
				allRef_time.get(currentIndex_mapping).add(time_db);
				all_sumDistance.get(currentIndex_mapping).add(sumDistance_db);
				all_speedKm.get(currentIndex_mapping).add(speed_km);
				all_speedInM.get(currentIndex_mapping).add(currentSpeed);

				sub_index = all_mean.get(currentIndex_mapping).size() - 1;

				if (sub_index < WINDOW_SIZE-1) {
					window_mean.get(currentIndex_mapping).add(0.0);
					diffMean.get(currentIndex_mapping).add(0.0);
				} else {
					int tempIndex = 0;
					for (int w = (sub_index - WINDOW_SIZE) + 1; w <= sub_index; w++) {
						window_sum[tempIndex] = all_sum.get(currentIndex_mapping).get(w);
						tempIndex++;
					}
					
					window_mean.get(currentIndex_mapping).add(getMean(window_sum));
					diffMean.get(currentIndex_mapping).add(all_mean.get(currentIndex_mapping).get(sub_index) - window_mean.get(currentIndex_mapping).get(sub_index));
				}
			}

			// set new index of diff_mean
			for (int source_index = 0; source_index < sourceId_mapping.size(); source_index++) {
				for (int rm = 0; rm < WINDOW_SIZE / 2; rm++) {
					diffMean.get(source_index).remove(rm);
				}
			}

			try {
				String folderName = "v2/";
				String fileName1 = folderName + "10_time.txt";
				String fileName2 = folderName + "10_diffmean.txt";
				String fileName3 = folderName + "10_sumdistance.txt";
				String fileName4 = folderName + "10_speedKm.txt";
				String fileName5 = folderName + "10_speedM.txt";

				BufferedWriter output1 = new BufferedWriter(new FileWriter(new File(fileName1)));
				BufferedWriter output2 = new BufferedWriter(new FileWriter(new File(fileName2)));
				BufferedWriter output3 = new BufferedWriter(new FileWriter(new File(fileName3)));
				BufferedWriter output4 = new BufferedWriter(new FileWriter(new File(fileName4)));
				BufferedWriter output5 = new BufferedWriter(new FileWriter(new File(fileName5)));

				for (int m = 0; m < diffMean.size(); m++) {
					for (int n = 0; n < diffMean.get(m).size(); n++) {
						output1.write(allRef_time.get(m).get(n) + System.getProperty( "line.separator" )  );
						output2.write(diffMean.get(m).get(n).toString() + System.getProperty( "line.separator" )  );
						output3.write(all_sumDistance.get(m).get(n) + System.getProperty( "line.separator" )  );
						output4.write(all_speedKm.get(m).get(n) + System.getProperty( "line.separator" )  );
						output5.write(all_speedInM.get(m).get(n) + System.getProperty( "line.separator" )  );
					}
					output1.write("-1" + System.getProperty( "line.separator" ));
					output2.write("-1" + System.getProperty( "line.separator" ));
					output3.write("-1" + System.getProperty( "line.separator" ));
					output4.write("-1" + System.getProperty( "line.separator" ));
					output5.write("-1" + System.getProperty( "line.separator" ));
				}
				
				output1.close();
				output2.close();
				output3.close();
				output4.close();
				output5.close();
				
				System.out.println("OK");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;
		} finally {
			try {
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static double getMean(ArrayList<Double> dataSet) {
		double sum = 0.0;
		for (double value : dataSet)
			sum += value;
		return sum / dataSet.size();
	}
	
	public static double getMean(double[] dataSet) {
		double sum = 0.0;
		for (double value : dataSet)
			sum += value;
		return sum / dataSet.length;
	}
}