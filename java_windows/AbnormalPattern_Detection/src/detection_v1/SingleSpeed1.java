package detection_v1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SingleSpeed1 {

	public static void main(String[] args) throws SQLException {

		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "chaiyaphum";
		String password = "14112534x";

		int WINDOW_SIZE = 6;

		String speedTable_name = "gen.\"incident_1h\"";
		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		String sqlStatemen;

		// detection variable
		ArrayList<ArrayList<Double>> diffMean = new ArrayList<ArrayList<Double>>();
		diffMean.add(new ArrayList<Double>());

		// all viriable
		ArrayList<ArrayList<Double>> all_sum = new ArrayList<ArrayList<Double>>();
		all_sum.add(new ArrayList<Double>());

		ArrayList<ArrayList<Double>> all_mean = new ArrayList<ArrayList<Double>>();
		all_mean.add(new ArrayList<Double>());

		// window variable
		ArrayList<ArrayList<Double>> window_mean = new ArrayList<ArrayList<Double>>();
		window_mean.add(new ArrayList<Double>());
		
		double[] window_sum = new double[WINDOW_SIZE];

		int index = 0;
		double currentSpeed;

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			sqlStatemen = "SELECT * FROM " + speedTable_name
					+ "  where id=34 order by source_time";

			pst1 = connection1.prepareStatement(sqlStatemen);
			rs1 = pst1.executeQuery();

			while (rs1.next()) {
				currentSpeed = Double.parseDouble(rs1.getString("speed"));
				all_sum.get(0).add(currentSpeed);
				all_mean.get(0).add(getMean(all_sum.get(0)));

				if (index < WINDOW_SIZE-1) {
					window_mean.get(0).add(0.0);
					diffMean.get(0).add(0.0);
				} else {
					int tempIndex = 0;
					for(int w=(index - WINDOW_SIZE)+1; w<=index; w++) {
						window_sum[tempIndex] = all_sum.get(0).get(w);
						tempIndex++;
					}
					
					window_mean.get(0).add(getMean(window_sum));
					diffMean.get(0).add(all_mean.get(0).get(index) - window_mean.get(0).get(index));
				}
				index++;
			}

			System.out.println(diffMean.get(0));
			//System.out.println(all_mean.get(0));
			//System.out.println(window_mean.get(0));

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;
		} finally {
			try {
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static double getMean(ArrayList<Double> dataSet) {
		double sum = 0.0;
		for (double value : dataSet)
			sum += value;
		return sum / dataSet.size();
	}
	
	public static double getMean(double[] dataSet) {
		double sum = 0.0;
		for (double value : dataSet)
			sum += value;
		return sum / dataSet.length;
	}
}