package groovenetdata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FilterData_intiInsert {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "chaiyaphum";
		String password = "14112534x";

		String sourceTable_name = "gipps_laneblock_filter.\"backup_filter_speedRange_4\"";
		String targetTable_name = "gipps_laneblock_filter.\"filter_speedRange_4\"";
		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		String sqlStatemen;

		String timeCurrent = null, sourceIdCurrent = null, laneStateCurrent = null;
		Double latCurrent = null, lonCurrent = null, headingCurrent = null, speedCurrent = null, time_inti = null;
		int count = 0;

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			sqlStatemen = "SELECT * FROM " + sourceTable_name + ";";
			pst1 = connection1.prepareStatement(sqlStatemen);
			rs1 = pst1.executeQuery();

			while (rs1.next()) {

				//if (count == 1) break; 
				timeCurrent = rs1.getString("source_time").substring(0, 10);
				sourceIdCurrent = rs1.getString("source_id");
				latCurrent = Double.parseDouble(rs1.getString("lat"));
				lonCurrent = Double.parseDouble(rs1.getString("lon"));
				speedCurrent = Double.parseDouble(rs1.getString("speed"));
				headingCurrent = Double.parseDouble(rs1.getString("heading"));
				laneStateCurrent = rs1.getString("lane_state");
				
				time_inti = Double.parseDouble(timeCurrent) - 1208982747;

				sqlStatemen = "INSERT INTO "
						+ targetTable_name
						+ "(source_time, source_id, lat, lon, speed, heading, lane_state, time_inti)VALUES (\'"
						+ timeCurrent + "\', \'" + sourceIdCurrent + "\',"
						+ latCurrent + "," + lonCurrent + "," + speedCurrent
						+ "," + headingCurrent + "," + laneStateCurrent + ","
						+ time_inti.intValue() + ");";

				//System.out.println(sqlStatemen);
				pst1 = connection1.prepareStatement(sqlStatemen);
				pst1.executeUpdate();
				count++;

			}

			System.out.println("Update : " + count + " statement");

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;
		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}