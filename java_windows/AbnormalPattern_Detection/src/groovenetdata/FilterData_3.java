package groovenetdata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FilterData_3 {

	public static void main(String[] args) {
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "chaiyaphum";
		String password = "14112534x";

		String sourceTable_name = "gipps_laneblock_filter.\"filter_speedRange_2\"";
		String targetTable_name = "gipps_laneblock_filter.\"filter_speedRange_3\"";
		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		String sqlStatemen;

		String timeOld = null, sourceIdOld = null, timeCurrent = null, sourceIdCurrent = null, laneStateCurrent = null;
		Double latCurrent = null, lonCurrent = null, headingCurrent = null, speedCurrent = null;
		int count = 0;
		boolean startFlag = true, normFlag = false;

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			sqlStatemen = "SELECT * FROM " + sourceTable_name
					+ " order by source_id asc, source_time desc;";
			pst1 = connection1.prepareStatement(sqlStatemen);
			rs1 = pst1.executeQuery();

			while (rs1.next()) {

				timeCurrent = rs1.getString("source_time").substring(0, 10);
				sourceIdCurrent = rs1.getString("source_id");
				latCurrent = Double.parseDouble(rs1.getString("lat"));
				lonCurrent = Double.parseDouble(rs1.getString("lon"));
				speedCurrent = Double.parseDouble(rs1.getString("speed"));
				headingCurrent = Double.parseDouble(rs1.getString("heading"));
				laneStateCurrent = rs1.getString("lane_state");

				if (!sourceIdCurrent.equals(sourceIdOld)) {
					startFlag = true;
					normFlag = false;
				}

				if (startFlag && speedCurrent > 100 && !normFlag) {
					startFlag = false;
					normFlag = true;
					
					sqlStatemen = "INSERT INTO "
							+ targetTable_name
							+ "(source_time, source_id, lat, lon, speed, heading, lane_state)VALUES (\'"
							+ timeCurrent + "\', \'" + sourceIdCurrent + "\',"
							+ latCurrent + "," + lonCurrent + ","
							+ speedCurrent + "," + headingCurrent + ","
							+ laneStateCurrent + ");";

					pst1 = connection1.prepareStatement(sqlStatemen);
					pst1.executeUpdate();
					count++;

					timeOld = timeCurrent;
					sourceIdOld = sourceIdCurrent;
				}

				if (normFlag) {
					sqlStatemen = "INSERT INTO "
							+ targetTable_name
							+ "(source_time, source_id, lat, lon, speed, heading, lane_state)VALUES (\'"
							+ timeCurrent + "\', \'" + sourceIdCurrent + "\',"
							+ latCurrent + "," + lonCurrent + ","
							+ speedCurrent + "," + headingCurrent + ","
							+ laneStateCurrent + ");";

					pst1 = connection1.prepareStatement(sqlStatemen);
					pst1.executeUpdate();
					count++;

					timeOld = timeCurrent;
					sourceIdOld = sourceIdCurrent;
				}
			}

			System.out.println("Update : " + count + "statement");

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;
		} finally {
			try {
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}