package groovenetdata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.io.*;
import java.util.*;

public class ReadTextFileToPostgres_Test_LaneBlock {

	public static void main(String[] args) throws IOException {

		String dir = "gg/";
		String fileName = "Test_LaneBlock_SingleLane_500veh.txt";
		String schemaName = "gipps_laneblock";
		String tableName = "singleLane_500veh";

		InputStream fis;
		BufferedReader br;
		String line;
		Connection con = null;
		PreparedStatement pst = null;
		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "chaiyaphum";
		String password = "14112534x";

		String[] main_seg;
		String sqlStatemen;

		try {
			Double speedInKm;
			con = DriverManager.getConnection(url, user, password);

			fis = new FileInputStream(dir + fileName);
			br = new BufferedReader(new InputStreamReader(fis,
					Charset.forName("UTF-8")));

			int i = 0;
			while ((line = br.readLine()) != null) {
				main_seg = (line.trim()).split(" +");

				speedInKm = Double.parseDouble(main_seg[4]) * 1.609344;

				sqlStatemen = "INSERT INTO "
						+ schemaName
						+ ".\""
						+ tableName
						+ "\"(source_time, source_id, lat, lon, speed, heading, lane_state)VALUES (\'"
						+ main_seg[0] + "\', \'" + main_seg[1] + "\',"
						+ main_seg[3] + "," + main_seg[2] + "," + speedInKm
						+ "," + main_seg[5] + "," + main_seg[6] + ");";
				pst = con.prepareStatement(sqlStatemen);
				pst.executeUpdate();
				i++;
			}
			System.out.println("insert -> " + i + "rows");

		} catch (SQLException ex) {
			System.out.println(ex);
			return;
		} finally {
			try {
				if (pst != null) {
					pst.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException ex) {
				return;
			}
		}
	}

}
