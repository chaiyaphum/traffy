package v1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Acceleration_WindowsCheck {

	static ArrayList<Double> speedList = new ArrayList<Double>();
	static ArrayList<Double> accelerationList = new ArrayList<Double>();
	static ArrayList<Double> cusumList = new ArrayList<Double>();

	static int diff_time = 20; // define in second

	public static void main(String[] args) {
		int window_size = 3;

		readFile("testCast/ab_5.txt");
		convertSpeedToAcceleration(speedList);
		getCuSumList(accelerationList);

		for (int i = 0; i < cusumList.size() - (window_size - 1); i += window_size) {
			double[] tempCuSum = new double[window_size];
			for (int j = 0; j < window_size; j++) {
				tempCuSum[j] = cusumList.get(i + j);
			}

			for (int k = 0; k < window_size; k++) {
				System.out.println((cusumList.get(i+k)) + "\t"
						+ (getMean(tempCuSum) + getStdDev(tempCuSum)) + "\t"
						+ (getMean(tempCuSum) - getStdDev(tempCuSum)));
			}
		}

	}

	public static void getCuSumList(ArrayList<Double> dataSet) {
		for (int i = 0; i < accelerationList.size(); i++) {
			ArrayList<Double> tempAcclerelation = new ArrayList<Double>();

			for (int j = 0; j <= i; j++) {
				tempAcclerelation.add(accelerationList.get(j));
			}
			cusumList.add(getCuSum(tempAcclerelation));
		}
	}

	public static double getCuSum(ArrayList<Double> dataSet) {
		double sum = 0.0;
		for (double value : dataSet) {
			sum += value;
		}

		return sum;
	}

	public static double getMean(double[] tempCuSum) {
		double sum = 0.0;
		for (double value : tempCuSum) {
			sum += value;
		}

		return sum / tempCuSum.length;
	}

	public static double getVariance(double[] tempCuSum) {
		double mean = getMean(tempCuSum);
		double temp = 0;
		for (double value : tempCuSum) {
			temp += (mean - value) * (mean - value);
		}

		return temp / (tempCuSum.length - 1);
	}

	public static double getStdDev(double[] tempCuSum) {
		return Math.sqrt(getVariance(tempCuSum));
	}

	public static double convertSpeedFromeMtoKm(double speedInKm) {
		return speedInKm * (5.0 / 18);
	}

	public static double getAcceleration(double speed_0, double speed_1,
			int diff_time) {
		return (speed_1 - speed_0) / diff_time;
	}

	public static void convertSpeedToAcceleration(ArrayList<Double> dataSet) {
		accelerationList.clear();
		for (int i = 1; i < dataSet.size(); i++) {
			accelerationList.add(getAcceleration(dataSet.get(i - 1),
					dataSet.get(i), diff_time));
		}
	}

	public static void readFile(String fileName) {
		try {
			File file = new File(fileName);
			Scanner scanner = new Scanner(file);
			while (scanner.hasNext()) {
				speedList
						.add(convertSpeedFromeMtoKm((double) scanner.nextInt()));
				// stats.addValue(scanner.nextInt());
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}