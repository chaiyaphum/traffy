package p;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;
import java.util.*;

public class Export {
	public static void main(String[] args) throws IOException {
		/*
		 * String host = "jdbc:postgresql://203.185.67.163:5432/Traffic_Data";
		 * String user = "chaiyaphum"; String pass = "qmlh14112534xyz";
		 */

		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		ArrayList<String> tableList = new ArrayList<String>();
		String filePath = null;
		String table_name = null;
		Connection connection1 = null;
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;

		String tempS = null;
		int count = 0;

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		}

		try {
			connection1 = DriverManager.getConnection(url, user, password);

			Scanner readTable = new Scanner(new FileReader("table.txt"));
			while (readTable.hasNextLine()) {
				tableList.add(readTable.nextLine());
			}

			for (int i = 0; i < tableList.size(); i++) {
				table_name = tableList.get(i);
				//filePath = "sql/" + table_name + ".txt";
				filePath = table_name + ".txt";
				BufferedWriter w = new BufferedWriter(new FileWriter(filePath));

				// String sql =
				// "SELECT source_id, date_time, lat, long, speed, heading, appid FROM probe_child3."
				// + table_name + ";\n";
				String sql = "SELECT * FROM gps.gps20130314 where (lat between 13.599769 and 13.648539) and  (long between 100.411971 and 100.684815);";
				pst1 = connection1.prepareStatement(sql);
				rs1 = pst1.executeQuery();

				while (rs1.next()) {
					count++;
					tempS = "INSERT INTO gps.\""
							+ table_name
							+ "\"(source_id,date_time,lat,\"long\",speed,heading,appid)VALUES ('"
							+ rs1.getString("source_id") + "','"
							+ rs1.getString("date_time") + "',"
							+ rs1.getDouble("lat") + ","
							+ rs1.getDouble("long") + "," + rs1.getInt("speed")
							+ "," + rs1.getInt("heading") + ","
							+ rs1.getInt("appid") + ");";
					w.write(tempS);
					w.newLine();
					readTable.close();
				}
				System.out.println("Dump : " + table_name);
				w.close();
			}

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;

		} finally {
			try {
				System.out.println("Export " + count + " row -> OK!!!");
				rs1.close();
				pst1.close();
				connection1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
