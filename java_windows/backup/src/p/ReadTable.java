package p;

import java.io.*;
import java.util.*;

public class ReadTable {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner readTable = new Scanner(new FileReader("table.txt"));
		ArrayList tableList = new ArrayList();
		String temp = null;
		
		while(readTable.hasNextLine()) {
			tableList.add(readTable.nextLine());
		}
		
		for(int i=0;i<tableList.size();i++) {
			temp = "CREATE TABLE gps." + tableList.get(i) + "(";
			temp += "source_id character varying(32),";
			temp += "date_time timestamp without time zone,";
			temp += "lat double precision,";
			temp += "long double precision,";
			temp += "speed double precision,";
			temp += "heading double precision,";
			temp += "appid character varying(8))";
			temp += "WITH (OIDS=FALSE);";
			temp += "ALTER TABLE gps." + tableList.get(i);
			temp += " OWNER TO postgres;";

			System.out.println(temp);
		}
		readTable.close();
	}
}
