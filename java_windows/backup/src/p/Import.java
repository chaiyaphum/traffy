package p;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.io.*;
import java.util.*;

public class Import {

	public static void main(String[] args) throws FileNotFoundException {
		Connection con = null;
		PreparedStatement pst = null;

		String url = "jdbc:postgresql://localhost:5433/traffy";
		String user = "postgres";
		String password = "14112534x";

		ArrayList<String> tableList = new ArrayList<String>();
		String table_name = null;
		String filePath = null;

		try {
			con = DriverManager.getConnection(url, user, password);

			Scanner readTable = new Scanner(new FileReader("table2.txt"));
			while (readTable.hasNextLine()) {
				tableList.add(readTable.nextLine());
			}

			for (int i = 0; i < tableList.size(); i++) {
				table_name = tableList.get(i);
				filePath = "" + table_name + ".txt";
				
				Scanner read = new Scanner(new FileReader(filePath));
				System.out.print("Import : " + table_name);
				while (read.hasNextLine()) {
					pst = con.prepareStatement(read.nextLine());
					pst.executeUpdate();
				}
				System.out.print(" --> OK : " + table_name + "\n");
				read.close();
			}
			readTable.close();
		} catch (SQLException ex) {
			System.out.println(ex);
			return;
		} finally {
			try {
				if (pst != null) {
					pst.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException ex) {
				return;
			}
		}
	}
}